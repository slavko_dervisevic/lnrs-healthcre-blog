<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage LexisNexis_WP_Theme
 * @since LexisNexis WP Theme 1.0
 */

get_header(); ?>

<script type="text/javascript">var switchTo5x=true;</script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({publisher: "e827bd5f-ea78-49d1-8d5a-4e680e08cc63", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>

<style>
#main {
  background: none;
}
.single.single-post .hentry {
	border-bottom: none;
}
</style>

<?php the_post(); ?>

<div class="breadcrumb-stripe">
  <div class="container">
  	<?php if(function_exists('simple_breadcrumb')) {simple_breadcrumb();} ?>
  </div>
</div>

<div id="main">

<div class="container">
<div class="row">
<div id="primary" class="col-md-9">

  <div class="single-wrapper">
  <div id="single-header">
    <h1 class="entry-title"><?php the_title(); ?></h1>
  </div>
    <div id="content" role="main">
      <div class="author">
        <?php
        $article_author_name = get_field('author', $args['id']);
        while (have_rows('experts', 'option')) {
          the_row();
          $name = get_sub_field('name');
          if ($name == $article_author_name) {
            $thumbnail = get_sub_field('thumbnail');
            if ($thumbnail):
            ?>
            <div class="wb-image"><img src="<?php echo esc_url($thumbnail['url']); ?>" alt="<?php echo esc_attr($name); ?>" class="photo" /></div>
            <?php
            endif;

            $author_name_and_title = $name;
            $author_title = get_sub_field('job_title');
            if ($author_title) $author_name_and_title .= ',<br /> ' . $author_title;

            $author_page = get_field('our_experts_page', 'options');
            
            if ($author_page && get_sub_field('visible')) {
              // Author page must be specified and the author visible on it:
              echo '<div class="wb"><a href="' . esc_url($author_page . '#' . urlencode($name)) . '" rel="author">' . $author_name_and_title . '</a><br /></div>';
            }
            else {
              // This user is not flagged (in Users administration) to be displayed on the Experts page.  Or there is no Experts page:
              echo '<div class="wb">' . $author_name_and_title . "</div>";
            }
            
          }
        }
        echo '<br /><div class="posted-on">' . get_the_date() . '</div>';
        ?>
   	  </div>

  <div class="social-share" style="margin-bottom: 20px;">
    <span class="st_facebook_hcount"></span>
    <span class="st_linkedin_hcount" st_image="http://www.lexisnexis.com/risk/img/logo-lexisnexis.png"></span>
    <span class="st_googleplus_hcount"></span>
    <span class="st_email_hcount"></span>
  </div>



      <?php get_template_part('content', 'single'); ?>
    </div><!-- #content -->
  </div><!-- #primary -->
      </div> <!-- single-wrapp -->

    <?php get_sidebar(); ?>
</div> <!-- row -->
</div> <!-- container -->

<div class="comments-stripe">
      <?php comments_template('', true); ?>
</div> <!-- comments stripe -->

<div class="contact-stripe">
<div class="container">
      Contact Us
</div> <!-- container -->
</div> <!-- comments stripe -->

<div class="contact-form-stripe">
<div class="container">
  <div class="contact-form">
      <?php include_once 'contactform/contact-form.html'; ?>
  </div>
</div> <!-- container -->
</div> <!-- comments stripe -->

<!-- #content -->

<?php get_footer(); ?>
