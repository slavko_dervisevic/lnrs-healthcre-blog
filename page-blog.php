<?php
/**
 * Template Name: Blog
 *
 * @package WordPress
 * @subpackage LexisNexis_WP_Theme
 */

get_header();
?>

<div id="primary">
    <div id="content" role="main">

        <article style="border: none !important;" id="post-<?php the_post(); the_ID(); ?>" <?php post_class(); ?>>
            <header class="entry-header">
                <h1 class="entry-title"><?php the_title(); ?></h1>
            </header>
            <!-- .entry-header -->

            <div class="entry-content">

<?php
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    query_posts(array('paged' => $paged));
    if (have_posts()) : ?>

        <?php //lnwptheme_content_nav('nav-above'); ?>

        <?php /* Start the Loop */ ?>
        <?php while (have_posts()) : the_post(); ?>

            <?php

            get_template_part('content', 'excerpt');

            ?>

            <?php endwhile; ?>

        <?php lnwptheme_content_nav('nav-below'); ?>

        <?php else : ?>

        <article id="post-0" class="post no-results not-found">
            <header class="entry-header">
                <h1 class="entry-title"><?php _e('Nothing Found', 'lnwptheme'); ?></h1>
            </header>
            <!-- .entry-header -->

            <div class="entry-content">
                <p><?php _e('Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'lnwptheme'); ?></p>
                <?php get_search_form(); ?>
            </div>
            <!-- .entry-content -->
        </article><!-- #post-0 -->

        <?php endif;
    wp_reset_query();?>

            </div>
        </article>

    </div>
    <!-- #content -->
</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>