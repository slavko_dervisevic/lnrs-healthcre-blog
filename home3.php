<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * Template Name: Home3
 * @package WordPress
 * @subpackage LexisNexis_WP_Theme
 */

get_header();?>
<script src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.matchHeight.js"></script>
<script src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.cycle.all.js"></script>
<script src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.easing.1.3.js"></script>
<script>
var firstTimeHomeSliderRuns = true;


(function($) {
  $(function() {
    $('#slider').cycle({
        fx:'fade',
        /*easing: 'easeInOutExpo',*/
        speed:1000,
        /*before: slideRebBlockLeft,*/
        timeout:8000,
        pager:'#slider-pager'
    });

    $('#sponsorships .rotator').show().cycle({
      fx:'fade',
      timeout:5000
    });

    $('.home-item').matchHeight();
  });
}(jQuery));
</script>
<?php
query_posts(array('post_type' => 'slide', 'posts_per_page' => -1));
if (have_posts()) {
    ?>
<div id="slider-wrapper">
    <div id="slider">
        <?php
        while (have_posts()) {
            the_post();
            ?>
            <div class="slide slide-<?php the_ID(); ?>">
                <?php the_post_thumbnail(); ?>
                
                <div class="insight_bar">
                    <h3><?php the_title();?></h3>
        
                    <div class="text">
                        <?php echo get_post_meta(get_the_ID(), 'banner_text', true);?>
                    </div>
                    
                    <?php 
                    $url = get_post_meta(get_the_ID(), 'banner_url', true);
                    $target = get_post_meta(get_the_ID(), 'target', true);
                    $target = ($target ? ' target="' . $target . '"' : '');
                    if ($url) {
                        ?><div class="clear"></div>
                         <a href="<?php echo $url; ?>" <?php echo $target; ?>><span class="red-link-button">Learn More</span></a><?php
                    } ?>
                </div>
            </div>
            <?php } ?>
    </div>
        <div id="slider-pager"></div>
        <?php
    
    } else {
        echo '<div style="height: 30px;background: #fff;"></div>';
    }
    wp_reset_query();
    ?>
</div>


<div id="primary">

    <div id="content" role="main">
    
		<!--<?php if(function_exists('simple_breadcrumb')) {simple_breadcrumb();} ?>-->

        <article style="border: none !important;" id="post-<?php the_post(); the_ID(); ?>" <?php post_class(); ?>>
            <!--<header class="entry-header">
                <h1 class="entry-title"><?php the_title(); ?></h1>
            </header>
            <!-- .entry-header -->

            <div class="entry-content" style="padding-top:15px;">

                <?php
				if (empty($_SESSION['fotd_sort'])) {
                    $_SESSION['fotd_sort'] = 'DESC';
                }

                if (empty($_SESSION['fotd_sortby'])) {
                    $_SESSION['fotd_sortby'] = 'date';
                }
				
				if ($_GET['fotd_sortby'] != NULL && $_SESSION['fotd_sortby'] != $_GET['fotd_sortby']) {
					if ($_GET['fotd_sortby'] == 'date') {
						$_GET['fotd_sort'] = 'DESC';
					} else {
						$_GET['fotd_sort'] = 'ASC';
					}
				}
                if (esc_attr($_GET['fotd_sort'])) {
                    $_SESSION['fotd_sort'] = $sort = esc_attr($_GET['fotd_sort']);
                } else {
                    $sort = $_SESSION['fotd_sort'];
                }

                if (esc_attr($_GET['fotd_sortby'])) {
                    $_SESSION['fotd_sortby'] = $sortby = esc_attr($_GET['fotd_sortby']);
                } else {
                    $sortby = $_SESSION['fotd_sortby'];
                }
				
				$_SESSION['fotd_month'] = $fotd_month = preg_replace('/[^0-9]+/', '', (isset($_GET['fotd_month']) ? $_GET['fotd_month'] : NULL));
				$_SESSION['fotd_category'] = $fotd_category = preg_replace('/[^0-9]+/', '', (isset($_GET['fotd_category'])) ? $_GET['fotd_category'] : NULL);
				$_SESSION['fotd_location'] = $fotd_location = preg_replace('/[^-_a-zA-Z]+/', '', (isset($_GET['fotd_location'])) ? $_GET['fotd_location'] : NULL);
        
        // Both fields use IDs (or blank) and can thereby be filtered as numeric:
        $_SESSION['archive_author'] = $archive_author = preg_replace('/[^0-9]+/', '', (isset($_GET['archive_author'])) ? $_GET['archive_author'] : NULL);
        $_SESSION['archive_content_type'] = $archive_content_type = preg_replace('/[^0-9]+/', '', (isset($_GET['archive_content_type'])) ? $_GET['archive_content_type'] : NULL);
                ?>
                
				<!--<div style="margin-bottom:5px;">Select date, category, content type and author to narrow the archive. You can select one or all options.</div>-->
                
                <!--Start month drop down menu-->
                <?php	
					$months = $wpdb->get_results( $wpdb->prepare( "
						SELECT DISTINCT YEAR( post_date ) AS year, MONTH( post_date ) AS month
						FROM $wpdb->posts
						WHERE post_type = %s
						ORDER BY post_date DESC
					", 'post' ) );
					$month_count = count( $months );
			
					if ( !$month_count || ( 1 == $month_count && 0 == $months[0]->month )) {return;}
							
					$m = isset( $_GET['m'] ) ? (int) $_GET['m'] : 0;
				?>
                    <!--<select id='fotd_month' style="margin:0 15px 14px 0; width:150px;">
                        <option<?php selected( $m, 0 ); ?> value='0'><?php _e('All Dates'); ?></option>
				<?php
					foreach ( $months as $arc_row ) {
						if ( 0 == $arc_row->year ){continue;}

						$month = zeroise( $arc_row->month, 2 );
						$year = $arc_row->year;			
						printf("<option %s value='%s' " . ($fotd_month==esc_attr($arc_row->year . $month) ? 'selected="selected"' : '') . ">%s</option>\n",
							selected( $m, $year . $month, false ),
							esc_attr( $arc_row->year . $month ),
							$wp_locale->get_month( $month ) . " $year"
						);
					}
				?>
					</select>-->
				<!--End month drop down menu-->

				<!--Start category drop down menu
				<?php echo wp_dropdown_categories(array(
						'show_option_all' => __('All Categories'),
						'show_option_none' => '',
						'orderby' => 'name', 
						'order' => 'ASC',
						'show_last_update' => 0, 
						'show_count' => 0,
						'hide_empty' => 1, 
						'child_of' => 0,
						'exclude' => '', 
						'echo' => 0,
						'selected' => $fotd_category, 
						'hierarchical' => 0,
						'name' => 'fotd_category', 
						'id' => 'fotd_category',
						'class' => 'postform', 
						'depth' => 0,
						'tab_index' => 0, 
						'taxonomy' => 'category',
						'hide_if_empty' => true
					)); 
				?>
                <style>select#fotd_category{width:220px;}</style>
				<!--End category drop down menu
                  
                <br />
                
                <select id="archive_author" style="margin: 0 15px 14px 0; width: 150px;">
                  <option value="">All Authors</option>

<?php
// Note that this excludes other valid authoring roles such as 'editor' or 'contributor':
$users = get_users(array(
  'role' => 'author',
  'orderby' => 'display_name',
));
foreach($users as $user):
?>
                  <option value="<?php echo $user->ID; ?>"<?php selected($archive_author, $user->ID); ?>><?php echo $user->data->display_name; ?></option>
<?php endforeach; ?>
                </select>
                
                <select id="archive_content_type" style="margin: 0 5px 14px 0; width: 220px;">
                  <option value="">All Content Types</option>
<?php
$terms = get_categories(array(
  'taxonomy' => 'content_types'
));
foreach($terms as $term):
?>
                  <option value="<?php echo $term->term_id; ?>"<?php selected($archive_content_type, $term->term_id); ?>><?php echo $term->name; ?></option>
<?php endforeach; ?>
                </select>
                
                <input type="button" id="fotd-filter-btn" class="button-secondary" style="margin:0 0 14px 0;" value="Refresh"/>-->

                

                <div class="clear"></div>
                
                <?php the_content(); ?>   
                
                <table class="posts-table" cellspacing="0" style="margin:-10px 0 0 0;">
                    <!--<thead>
                    <tr>
                        <th>
                            <a <?php if ($sort == 'DESC' && $sortby == 'date') echo ' class="sort-down" '; else if ($sort == 'ASC' && $sortby == 'date') echo ' class="sort-up" '  ?>
                                href="?fotd_sort=<?php if ($sort == 'DESC') echo 'ASC'; else if ($sort == 'ASC') echo 'DESC'; ?>&fotd_sortby=date<?php echo (!is_null($fotd_month) ? '&fotd_month=' . $fotd_month : ''); ?><?php echo (!is_null($fotd_category) ? '&fotd_category=' . $fotd_category : ''); ?><?php echo (!is_null($fotd_location) ? '&fotd_location=' . $fotd_location : ''); ?><?php echo (!is_null($archive_author) ? '&archive_author=' . $archive_author : ''); ?><?php echo (!is_null($archive_content_type) ? '&archive_content_type=' . $archive_content_type : ''); ?>">Date</a>
                        </th>
                        <th>
							<a <?php if ($sort == 'DESC' && $sortby == 'headline') echo ' class="sort-down" '; else if ($sort == 'ASC' && $sortby == 'headline') echo ' class="sort-up" '  ?>
                                href="?fotd_sort=<?php if ($sort == 'DESC') echo 'ASC'; else if ($sort == 'ASC') echo 'DESC'; ?>&fotd_sortby=headline<?php echo (!is_null($fotd_month) ? '&fotd_month=' . $fotd_month : ''); ?><?php echo (!is_null($fotd_category) ? '&fotd_category=' . $fotd_category : ''); ?><?php echo (!is_null($fotd_location) ? '&fotd_location=' . $fotd_location : ''); ?><?php echo (!is_null($archive_author) ? '&archive_author=' . $archive_author : ''); ?><?php echo (!is_null($archive_content_type) ? '&archive_content_type=' . $archive_content_type : ''); ?>">Headline</a>
						</th>
                        <th>
                            <a <?php if ($sort == 'DESC' && $sortby == 'cat') echo ' class="sort-down" '; else if ($sort == 'ASC' && $sortby == 'cat') echo ' class="sort-up" '  ?>
                                href="?fotd_sort=<?php if ($sort == 'DESC') echo 'ASC'; else if ($sort == 'ASC') echo 'DESC'; ?>&fotd_sortby=cat<?php echo (!is_null($fotd_month) ? '&fotd_month=' . $fotd_month : ''); ?><?php echo (!is_null($fotd_category) ? '&fotd_category=' . $fotd_category : ''); ?><?php echo (!is_null($fotd_location) ? '&fotd_location=' . $fotd_location : ''); ?><?php echo (!is_null($archive_author) ? '&archive_author=' . $archive_author : ''); ?><?php echo (!is_null($archive_content_type) ? '&archive_content_type=' . $archive_content_type : ''); ?>">Category</a>
                        </th>
                    </tr>
                    </thead> -->
                    <tbody>
                    <?php
                    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                    $params = array(
						'paged'   => $paged,
                        'order'   => $sort,
                        'orderby' => ($sortby == 'date' ? $sortby : ($sortby == 'headline' ? 'title' : 'meta_value'))
                    );
                    if ($sortby == 'cat') {
                        $params['meta_key'] = 'category';
                    }
                    if ($sortby == 'state') {
                        $params['meta_key'] = 'state-national';
                    }
                    // Date filter:
          					if (preg_match('/^[0-9]{6}+$/', $fotd_month)){
          						$params['year'] = substr($fotd_month, 0, 4);
          						$params['monthnum'] = substr($fotd_month, 4, 2);
          					}
                    // Category filter:
          					if (preg_match('/^[0-9]+$/', $fotd_category)){
          						$params['category__and'] = $fotd_category;
          					}
                    // Author filter:
          					if (preg_match('/^[0-9]+$/', $archive_author)) {
                      $params['author'] = $archive_author;
                    }
                    
                    // Content Type ('content_type' taxonomy term) filter:
          					if (preg_match('/^[0-9]+$/', $archive_content_type)) {
                      $params['tax_query'] = array(array(
                        'taxonomy' => 'content_types',
                        'field' => 'term_id',
                        'terms' => $archive_content_type
                      ));
                    }

                    $posts = query_posts($params);
					//echo $GLOBALS['wp_query']->request;
                    ?>

                    <?php
                    $odd = 0;
					if (count($posts) > 0){
						while (have_posts()) : the_post();
	
							$categories = wp_get_post_categories(get_the_ID());
							$states = wp_get_post_terms(get_the_ID(), 'state-national');
							if (count($categories) > 0) {
								$categories = get_the_category_by_ID($categories[0]);
							}
							$url = get_permalink(get_the_ID());//stripslashes_deep(get_post_meta(get_the_ID(), 'url', true));
							?>
						<tr <?php if ($odd++ % 2 == 0) echo 'class="even"'; else echo 'class="odd"';  ?>>
							<td style="width:170px;">
								<div align="center"><?php the_post_thumbnail( array(161,100) ); ?></div>
							</td>
							<td class="title">
                <div><a href="<?php if (empty($url)) { the_permalink(); } else { echo $url; } ?>"><?php the_title(); ?></a></div>
                <div><?php render_written_by(false, true); ?></div><br>
				<div style="font-weight:bold"><?php the_time('F j, Y'); ?> | <?php echo $categories; ?></div>
              </td>
							
						</tr>
							<?php
						endwhile;
					} else { ?>
                    	<tr>
                        	<td colspan="4">There are no articles for the month, category, author and content type options that you selected. Please choose different criteria.</td>
                        </tr>
                        
					<?php } ?>
                    </tbody>
                </table>

                


            </div>
        </article>

    </div>
    <!-- #content -->
</div><!-- #primary -->


<?php get_sidebar(); ?>

<?php get_footer(); ?>