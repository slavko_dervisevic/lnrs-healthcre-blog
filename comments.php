<?php
/**
 * The template for displaying Comments.
 *
 * The area of the page that contains both current comments
 * and the comment form. The actual display of comments is
 * handled by a callback to lnwptheme_comment() which is
 * located in the functions.php file.
 *
 * @package WordPress
 * @subpackage LexisNexis_WP_Theme
 * @since LexisNexis WP Theme 1.0
 */
?>
<div id="comments">
	<div class="comments-wrapper">
    <div class="container">
    <div class="row">
    <div class="col-md-9">
            <div class="comments-header">
                post a comment
            </div>    
	<?php if (post_password_required()) : ?>
    <p class="nopassword"><?php _e('This post is password protected. Enter the password to view any comments.', 'lnwptheme'); ?></p>
	</div><!-- #comments -->
<?php
 /* Stop the rest of comments.php from being processed,
			 * but don't kill the script entirely -- we still have
			 * to fully load the template.
			 */
    return;
endif;
?>

<?php // You can start editing here -- including this comment! ?>
        <div id="form-comment-block">
            <?php comment_form(array('title_reply' => 'Post a Comment', 'title_reply_to' => 'Post a Comment')); ?>
        </div>
    </div><!-- col-->
    </div><!-- row-->
    </div><!-- container-->
    </div><!-- wrapper (gray bg) -->

<div class="comments-list">
<div class="container">
<div class="row">
<div class="col-md-9">
<?php if (have_comments()) : ?>
<?php  /*<h2 id="comments-title">
			<?php
				printf( _n( 'One thought on &ldquo;%2$s&rdquo;', '%1$s thoughts on &ldquo;%2$s&rdquo;', get_comments_number(), 'lnwptheme' ),
					number_format_i18n( get_comments_number() ), '<span>' . get_the_title() . '</span>' );
			?>
		</h2>*/
    ?>

<?php if (get_comment_pages_count() > 1 && get_option('page_comments')) : // are there comments to navigate through ?>
    <nav id="comment-nav-above">
        <h1 class="assistive-text"><?php _e('Comment navigation', 'lnwptheme'); ?></h1>

        <div class="nav-previous"><?php previous_comments_link(__('&larr; Older Comments', 'lnwptheme')); ?></div>
        <div class="nav-next"><?php next_comments_link(__('Newer Comments &rarr;', 'lnwptheme')); ?></div>
    </nav>
    <?php endif; // check for comment navigation ?>

<ol class="commentlist">
    <?php
                    /* Loop through and list the comments. Tell wp_list_comments()
				 * to use lnwptheme_comment() to format the comments.
				 * If you want to overload this in a child theme then you can
				 * define lnwptheme_comment() and that will be used instead.
				 * See lnwptheme_comment() in lnwptheme/functions.php for more.
				 */
    wp_list_comments(array('callback' => 'lnwptheme_comment'));
    ?>
</ol>

<?php if (get_comment_pages_count() > 1 && get_option('page_comments')) : // are there comments to navigate through ?>
    <nav id="comment-nav-below">
        <h1 class="assistive-text"><?php _e('Comment navigation', 'lnwptheme'); ?></h1>

        <div class="nav-previous"><?php previous_comments_link(__('&larr; Older Comments', 'lnwptheme')); ?></div>
        <div class="nav-next"><?php next_comments_link(__('Newer Comments &rarr;', 'lnwptheme')); ?></div>
    </nav>
    <?php endif; // check for comment navigation ?>

    <?php
 /* If there are no comments and comments are closed, let's leave a little note, shall we?
		 * But we don't want the note on pages or post types that do not support comments.
		 */
elseif (!comments_open() && !is_page() && post_type_supports(get_post_type(), 'comments')) :
    ?>
<p class="nocomments"><?php _e('Comments are closed.', 'lnwptheme'); ?></p>
<?php endif; ?>
<script>
jQuery(document).ready(function () {
	jQuery('.comment-form-url label').html('Agency/Organization');
});
</script>
</div><!-- #comments-list -->
</div><!-- #col -->
</div><!-- row -->
</div><!-- #container -->

</div><!-- #comments -->
