<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package WordPress
 * @subpackage LexisNexis_WP_Theme
 * @since LexisNexis WP Theme 1.0
 */
?>

</div><!-- #main -->


</div><!-- #page -->

<footer id="colophon" role="contentinfo">
	<div class="footer-1">
	<div class="container">
		<div id="footer-menu">
		  <?php wp_nav_menu(array('link_after'=>'&nbsp;&nbsp;<span class="basic-divider">|</span>&nbsp;&nbsp;'));  ?>
		  <div class="clear"></div>
		</div>
		<div class="copyright">
		  <p><a href="https://www.lexisnexis.com/terms/copyright.aspx">Copyright &copy;</a>
		  <?php echo date('Y') ?> LexisNexis.</p>
		  <p>These blogs are published for information purposes only and can be
		  statements of opinion. Although we LexisNexis rigorously check the
		  accuracy of all information at the time of publishing the blogs, no
		  representations or warranties are expressed or implied as to the blog, its
		  contents and any accompanying materials and it should not be relied upon
		  for acting in specific circumstances. Although links to external websites
		  on any blog posts are tested and deemed accurate at the time of the blog
		  posting, we LexisNexis accept no liability for such links to external
		  websites and do not endorse or warrant in any way any materials available
		  through such links or any privacy or other practices of such sites. In
		  addition to this blog disclaimer, access and use of the blogs is governed
		  by the LexisNexis website.</p>
		</div>
    </div>
	</div>
	<div class="footer-2">
	<div class="container">
		<div class="row">
				<div class="col-md-3">
				
					<div id="connect-with-us">
						<div class="links">
							<a class="linkedin" title="LinkedIn" href="https://www.linkedin.com/showcase/lexisnexis-risk-solutions-health-care/" target="_blank"></a>
						</div>
						<div class="links">
							<a class="twitter" title="Twitter" href="https://twitter.com/LexisHealthCare" target="_blank"></a>
						</div>
						<div class="links">
							<a class="facebook" title="Facebook" href="https://twitter.com/LexisHealthCare" target="_blank"></a>
						</div>
						<div class="links">
							<a class="youtube" title="YouTube" href="https://www.youtube.com/playlist?list=PL7RiwvDcpa_5YWm8dJfOVtLgaNBfoEqut" target="_blank"></a>
						</div>
						<!--
						<div class="links">
							<a class="rss" title="RSS" href="/healthcare/feed/" target="_blank"></a>
						</div>
						-->
					</div>

				</div>
				<div class="col-md-6">
					<a class="terms-link" href="https://risk.lexisnexis.com/terms" target="_blank">Terms &amp; Conditions</a> 
					|
					<a class="privacy-link" href="https://risk.lexisnexis.com/privacy-policy" target="_blank">Privacy Policy</a>
				</div>
				<div class="col-md-3">
				<a href="https://www.relx.com" target="_blank"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/logo-relx-white.png" alt="" /></a>
				</div>
		</div>
    </div>
		
	</div>
</footer><!-- #colophon -->

<?php wp_footer(); ?>
<?php echo htmlspecialchars_decode(stripslashes_deep(get_option('lexisnexisblog-scripts-footer'))); ?>

<script type='text/javascript'>
jQuery(document).ready(function(){
	jQuery('.top-search').on('click','.opener',function(e){
		e.stopPropagation();
		jQuery('.top-search').addClass('open');
	});
			
	jQuery('body').on('click',function(e){
		$target = jQuery(e.target);
		if( jQuery('.top-search').hasClass('open') 
			&& !jQuery('.navbar-toggler').is(":visible")
			&& (!$target.closest('.top-search').length) ) {
        jQuery('.top-search').removeClass('open');
		}
	});
  
  jQuery('#comments .comments-header').on('click',function(){
    jQuery('#comments').toggleClass('open');
  });

  jQuery('.widget-about').on('mouseenter',function(){
    jQuery('.widget-about').addClass('open');
  });

  jQuery('.widget-about').on('mouseleave',function(){
    jQuery('.widget-about').removeClass('open');
  });

  if( jQuery('.article-grid').length ) {
    jQuery('.article-grid .tile .inner').matchHeight();
  }
/*
	$('.article-grid .tile .date').ellipsis({
		lines: 2,             // force ellipsis after a certain number of lines. Default is 'auto'
		ellipClass: 'ellip',  // class used for ellipsis wrapper and to namespace ellip line
		responsive: true      // set to true if you want ellipsis to update on window resize. Default is false
	});

  	$('.article-grid .tile .title').ellipsis({
		lines: 2,             // force ellipsis after a certain number of lines. Default is 'auto'
		ellipClass: 'ellip',  // class used for ellipsis wrapper and to namespace ellip line
		responsive: true      // set to true if you want ellipsis to update on window resize. Default is false
	});
*/
});
</script>

<!--BEGIN: : Tracking external, download, and email links -->

<script type='text/javascript'>
if (typeof jQuery != 'undefined') {
  var filetypes = /\.(zip|exe|dmg|pdf|doc.*|xls.*|ppt.*|mp3|txt|rar|wma|mov|avi|wmv|flv|wav)$/i;
  var baseHref = '';
  if (jQuery('base').attr('href') != undefined) baseHref = jQuery('base').attr('href');
  var hrefRedirect = '';

  jQuery('body').on('click', 'a', function(event) {
    var el = jQuery(this);
    var track = true;
    var href = (typeof(el.attr('href')) != 'undefined' ) ? el.attr('href') : '';
    var isThisDomain = href.match(document.domain.split('.').reverse()[1] + '.' + document.domain.split('.').reverse()[0]);
    if (!href.match(/^javascript:/i)) {
      var elEv = []; elEv.value=0, elEv.non_i=false;
      if (href.match(/^mailto\:/i)) {
        elEv.category = 'email';
        elEv.action = 'click';
        elEv.label = href.replace(/^mailto\:/i, '');
        elEv.loc = href;
      }
      else if (href.match(filetypes)) {
        var extension = (/[.]/.exec(href)) ? /[^.]+$/.exec(href) : undefined;
        elEv.category = 'download';
        elEv.action = 'click-' + extension[0];
        elEv.label = href.replace(/ /g,'-');
        elEv.loc = baseHref + href;
      }
      else if (href.match(/^https?\:/i) && !isThisDomain) {
        elEv.category = 'external';
        elEv.action = 'click';
        elEv.label = href.replace(/^https?\:\/\//i, '');
        elEv.non_i = true;
        elEv.loc = href;
      }
      else if (href.match(/^tel\:/i)) {
        elEv.category = 'telephone';
        elEv.action = 'click';
        elEv.label = href.replace(/^tel\:/i, '');
        elEv.loc = href;
      }
      else track = false;

      if (track) {
        var ret = true;

        if((elEv.category == 'external' || elEv.category == 'download') && (el.attr('target') == undefined || el.attr('target').toLowerCase() != '_blank') ) {
          hrefRedirect = elEv.loc;

          ga('send','event', elEv.category.toLowerCase(),elEv.action.toLowerCase(),elEv.label.toLowerCase(),elEv.value,{
            'nonInteraction': elEv.non_i ,
            'hitCallback':gaHitCallbackHandler
          });

          ret = false;
        }
        else {
          ga('send','event', elEv.category.toLowerCase(),elEv.action.toLowerCase(),elEv.label.toLowerCase(),elEv.value,{
            'nonInteraction': elEv.non_i
          });
        }

        return ret;
      }
    }
  });

  gaHitCallbackHandler = function() {
    window.location.href = hrefRedirect;
  }
}
</script>

<!--END: Tracking external, download, and email links -->


<!-- Eloqua Tracking Script: Start -->
<script type="text/javascript">
    var _elqQ = _elqQ || [];
    _elqQ.push(['elqSetSiteId', '903']);
    _elqQ.push(['elqTrackPageView']);

    (function () {
        function async_load() {
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true;
            s.src = '//img.en25.com/i/elqCfg.min.js';
            var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
        }
        if (window.addEventListener) window.addEventListener('DOMContentLoaded', async_load, false);
        else if (window.attachEvent) window.attachEvent('onload', async_load);
    })();
</script>
<!-- Eloqua Tracking Script: End -->




</body>
</html>
