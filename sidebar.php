<?php
/**
 * The Sidebar containing the main widget area.
 *
 * @package WordPress
 * @subpackage LexisNexis_WP_Theme
 * @since LexisNexis WP Theme 1.0
 */

// Helper functions:

function renderSpotlightPost($post) {

  $article_author_name = get_field('author', $post->ID);
  while (have_rows('experts', 'option')) {
    the_row();
    $name = get_sub_field('name');
    if ($name == $article_author_name) {
      $thumbnail = get_sub_field('thumbnail');
      if ($thumbnail) {
      } else {
        $thumbnail = '';
      }
      
    }
  }




?>
<div class="spotlight-item">
<div class="spotlight-thumbnail">
  <img src="<?php echo esc_url($thumbnail['url']); ?>" alt="<?php echo esc_attr($name); ?>" class="photo" />
</div>
<div class="spotlight-description">
  <p><a href="<?php echo get_permalink($post); ?>"><?php echo $post->post_title . get_the_expert(Array('show_title' => false, 'show_link' => false, 'id' => $post->ID, 'prefix' => ", By ")); ?></a><br /></p>
</div>
</div>
<?php
}

function renderPost($post) {
?>
<div style="clear: both;">
  <p><a href="<?php echo get_permalink($post); ?>"><?php echo $post->post_title . get_the_expert(Array('show_title' => false, 'show_link' => false, 'id' => $post->ID, 'prefix' => ", By ")); ?></a><br /></p>
</div>
<?php
}

function ln_is_home_page() {
  $slug = get_page_template_slug();
  // Everything other than the front page that we want to have homepage sidebars:
  return $slug == 'home2.php' || $slug == 'home3.php' || $slug == 'page-region-home.php';
}

// Mainline execution begins here:
?>
<div class="col-md-3">
<div id="secondary" class="widget-area" role="complementary">
<?php
wp_reset_query(); // Very important; the caller may have trashed the loop.

if (is_page()) {
  // Render the "All Pages" sidebar:
  dynamic_sidebar('sidebar-pages');

  if (is_front_page() || ln_is_home_page()) {
    global $wp_query;

    // Render Homepage Spotlight:
    if (have_rows('spotlight', $wp_query->post->ID)):
?>
<aside class="widget widget_spotlight">
  <h3 class="widget-title">Spotlight</h3>
  <div class="spotlight-body">
<?php
while (have_rows('spotlight', $wp_query->post->ID)): the_row();
  $post = get_sub_field('post');
  renderSpotlightPost($post);
endwhile;
?>
  </div>
</aside>
<?php
    endif; // End Homepage Spotlight

    // Render the rest of the homepage-specific sidebar widgets:
    dynamic_sidebar('homepage-sidebar');
  } // is_front_page()
} // is_page()
else if (is_single()) {
  dynamic_sidebar('sidebar-pages');
  dynamic_sidebar('sidebar-single');

  // Dynamic "Related Articles" section:
  // TODO: Should probably be built as a widget?
?>
    <aside class="widget widget_spotlight">
      <h3 class="widget-title">Related Articles</h3>
<?php
  global $wp_query;
  wp_reset_query();
  if (have_rows('related_articles', $wp_query->post->ID)) {
    while (have_rows('related_articles', $wp_query->post->ID)): the_row();
      $post = get_sub_field('post');
      renderPost($post);
    endwhile;
  }
  else {
    // No related articles are specified; find the three latest articles (that
    // aren't this article) within the category and render those instead.
    $categories = get_the_category();
    $recent_posts = get_posts(array(
      'category' => $categories[0]->cat_ID,
      'posts_per_page' => 3,
      'exclude' => $wp_query->post->ID,
    ));
    foreach ($recent_posts as $idx => $post) {
      renderPost($post);
    }
    wp_reset_query();
  }
?>
    </aside>
<?php
} // is_single()
else {
  dynamic_sidebar('sidebar-pages');
}

// If there is any ACF field named sidebar_content, display it at the end.
$sidebar_heading = get_field('sidebar_heading');
$sidebar_content = get_field('sidebar_content');
if ($sidebar_heading || $sidebar_content):
?>
<aside class="widget widget_sidebar_content">
  <?php if ($sidebar_heading): ?>
  <h3 class="widget-title"><?php echo $sidebar_heading; ?></h3>
  <?php endif; ?>
  <?php echo $sidebar_content; ?>
</aside>
<?php
endif;
?>
</div><!-- #secondary .widget-area -->
</div><!-- column -->
