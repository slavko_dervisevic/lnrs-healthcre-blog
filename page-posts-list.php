<?php
/**
 * Template Name: Posts List Table
 *
 * @package WordPress
 * @subpackage LexisNexis_WP_Theme
 */
global $wpdb, $wp_locale;

get_header();
?>

<style>
#fotd_category{
	margin:0 5px 14px 0;
}
</style>
<div id="primary">
    <div id="content" role="main">

    <?php if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('<p id="breadcrumbs">','</p>');
				} ?>

        <article style="border: none !important;" id="post-<?php the_post(); the_ID(); ?>" <?php post_class(); ?>>
            <header class="entry-header">
                <h1 class="entry-title"><?php the_title(); ?></h1>
            </header>
            <!-- .entry-header -->

            <div class="entry-content" style="padding-top:15px;">

                <?php
				if (empty($_SESSION['fotd_sort'])) {
                    $_SESSION['fotd_sort'] = 'DESC';
                }

                if (empty($_SESSION['fotd_sortby'])) {
                    $_SESSION['fotd_sortby'] = 'date';
                }

				if ($_GET['fotd_sortby'] != NULL && $_SESSION['fotd_sortby'] != $_GET['fotd_sortby']) {
					if ($_GET['fotd_sortby'] == 'date') {
						$_GET['fotd_sort'] = 'DESC';
					} else {
						$_GET['fotd_sort'] = 'ASC';
					}
				}
                if (esc_attr($_GET['fotd_sort'])) {
                    $_SESSION['fotd_sort'] = $sort = esc_attr($_GET['fotd_sort']);
                } else {
                    $sort = $_SESSION['fotd_sort'];
                }

                if (esc_attr($_GET['fotd_sortby'])) {
                    $_SESSION['fotd_sortby'] = $sortby = esc_attr($_GET['fotd_sortby']);
                } else {
                    $sortby = $_SESSION['fotd_sortby'];
                }

				$_SESSION['fotd_month'] = $fotd_month = preg_replace('/[^0-9]+/', '', (isset($_GET['fotd_month']) ? $_GET['fotd_month'] : NULL));
				$_SESSION['fotd_category'] = $fotd_category = preg_replace('/[^0-9]+/', '', (isset($_GET['fotd_category'])) ? $_GET['fotd_category'] : NULL);
				$_SESSION['fotd_location'] = $fotd_location = preg_replace('/[^-_a-zA-Z]+/', '', (isset($_GET['fotd_location'])) ? $_GET['fotd_location'] : NULL);
                ?>

				<div style="margin-bottom:5px;">Select date, category and location to narrow fraud archive. You can select one or all options.</div>

                <!--Start month drop down menu-->
                <?php
					$months = $wpdb->get_results( $wpdb->prepare( "
						SELECT DISTINCT YEAR( post_date ) AS year, MONTH( post_date ) AS month
						FROM $wpdb->posts
						WHERE post_type = %s
						ORDER BY post_date DESC
					", 'post' ) );
					$month_count = count( $months );

					if ( !$month_count || ( 1 == $month_count && 0 == $months[0]->month ) ){return;}

					$m = isset( $_GET['m'] ) ? (int) $_GET['m'] : 0;
				?>
                    <select id='fotd_month' style="margin:0 5px 14px 0; width:125px;">
                        <option<?php selected( $m, 0 ); ?> value='0'><?php _e('All Dates'); ?></option>
				<?php
					foreach ( $months as $arc_row ) {
						if ( 0 == $arc_row->year ){continue;}

						$month = zeroise( $arc_row->month, 2 );
						$year = $arc_row->year;
						printf("<option %s value='%s' " . ($fotd_month==esc_attr($arc_row->year . $month) ? 'selected="selected"' : '') . ">%s</option>\n",
							selected( $m, $year . $month, false ),
							esc_attr( $arc_row->year . $month ),
							$wp_locale->get_month( $month ) . " $year"
						);
					}
				?>
					</select>&nbsp;&nbsp;&nbsp;
				<!--End month drop down menu-->


				<!--Start category drop down menu-->
				<?php echo wp_dropdown_categories(array(
						'show_option_all' => __('All Categories'),
						'show_option_none' => '',
						'orderby' => 'name',
						'order' => 'ASC',
						'show_last_update' => 0,
						'show_count' => 0,
						'hide_empty' => 1,
						'child_of' => 0,
						'exclude' => '',
						'echo' => 0,
						'selected' => $fotd_category,
						'hierarchical' => 0,
						'name' => 'fotd_category',
						'id' => 'fotd_category',
						'class' => 'postform',
						'depth' => 0,
						'tab_index' => 0,
						'taxonomy' => 'category',
						'hide_if_empty' => true
					));
				?>&nbsp;&nbsp;&nbsp;
                <style>select#fotd_category{width:195px;}</style>
				<!--End category drop down menu-->


                <!--End location drop down menu-->
                <?php
				$terms = $wpdb->get_results("SELECT t.name, t.term_id, t.slug, tt.term_id FROM $wpdb->terms AS t INNER JOIN $wpdb->term_taxonomy AS tt ON t.term_id = tt.term_id WHERE tt.taxonomy = 'state-national' ORDER BY t.name", OBJECT_K);
				?>
                    <select id='fotd_location' style="margin:0 5px 14px 0;">
                        <option value='0'><?php _e('All Locations'); ?></option>

				<?php foreach ($terms as $k => $term){ echo var_dump($term); ?>
                		<option value="<?php echo $term->slug; ?>" <?php echo ($fotd_location==$term->slug ? 'selected="selected"' : ''); ?>><?php echo $term->name; ?></option
                ><?php } ?>

                	</select>
                <!--End location drop down menu-->


                <input type="button" id="fotd-filter-btn" class="button-secondary" style="margin:0 0 14px 0;" value="Search"/>
                <div class="clear"></div>

                <?php the_content(); ?>

                <table class="posts-table" cellspacing="0" style="margin:-10px 0 0 0;">
                    <thead>
                    <tr>
                        <th>
                            <a <?php if ($sort == 'DESC' && $sortby == 'date') echo ' class="sort-down" '; else if ($sort == 'ASC' && $sortby == 'date') echo ' class="sort-up" '  ?>
                                href="?fotd_sort=<?php if ($sort == 'DESC') echo 'ASC'; else if ($sort == 'ASC') echo 'DESC'; ?>&fotd_sortby=date<?php echo (!is_null($fotd_month) ? '&fotd_month=' . $fotd_month : ''); ?><?php echo (!is_null($fotd_category) ? '&fotd_category=' . $fotd_category : ''); ?><?php echo (!is_null($fotd_location) ? '&fotd_location=' . $fotd_location : ''); ?>">Date</a>
                        </th>
                        <th>
							<a <?php if ($sort == 'DESC' && $sortby == 'headline') echo ' class="sort-down" '; else if ($sort == 'ASC' && $sortby == 'headline') echo ' class="sort-up" '  ?>
                                href="?fotd_sort=<?php if ($sort == 'DESC') echo 'ASC'; else if ($sort == 'ASC') echo 'DESC'; ?>&fotd_sortby=headline<?php echo (!is_null($fotd_month) ? '&fotd_month=' . $fotd_month : ''); ?><?php echo (!is_null($fotd_category) ? '&fotd_category=' . $fotd_category : ''); ?><?php echo (!is_null($fotd_location) ? '&fotd_location=' . $fotd_location : ''); ?>">Headline</a>
						</th>
                        <th>
                            <a <?php if ($sort == 'DESC' && $sortby == 'cat') echo ' class="sort-down" '; else if ($sort == 'ASC' && $sortby == 'cat') echo ' class="sort-up" '  ?>
                                href="?fotd_sort=<?php if ($sort == 'DESC') echo 'ASC'; else if ($sort == 'ASC') echo 'DESC'; ?>&fotd_sortby=cat<?php echo (!is_null($fotd_month) ? '&fotd_month=' . $fotd_month : ''); ?><?php echo (!is_null($fotd_category) ? '&fotd_category=' . $fotd_category : ''); ?><?php echo (!is_null($fotd_location) ? '&fotd_location=' . $fotd_location : ''); ?>">Category</a>
                        </th>
                        <th>
                            <a <?php if ($sort == 'DESC' && $sortby == 'state') echo ' class="sort-down" '; else if ($sort == 'ASC' && $sortby == 'state') echo ' class="sort-up" '  ?>
                                href="?fotd_sort=<?php if ($sort == 'DESC') echo 'ASC'; else if ($sort == 'ASC') echo 'DESC'; ?>&fotd_sortby=state<?php echo (!is_null($fotd_month) ? '&fotd_month=' . $fotd_month : ''); ?><?php echo (!is_null($fotd_category) ? '&fotd_category=' . $fotd_category : ''); ?><?php echo (!is_null($fotd_location) ? '&fotd_location=' . $fotd_location : ''); ?>">State/National</a>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                    $params = array(
						'paged'   => $paged,
                        'order'   => $sort,
                        'orderby' => ($sortby == 'date' ? $sortby : ($sortby == 'headline' ? 'title' : 'meta_value'))
                    );
                    if ($sortby == 'cat') {
                        $params['meta_key'] = 'category';
                    }
                    if ($sortby == 'state') {
                        $params['meta_key'] = 'state-national';
                    }
					if (preg_match('/^[0-9]{6}+$/', $fotd_month)){
						$params['year'] = substr($fotd_month, 0, 4);
						$params['monthnum'] = substr($fotd_month, 4, 2);
					}
					if (preg_match('/^[0-9]+$/', $fotd_category)){
						$params['category__and'] = $fotd_category;
					}
					if (preg_match('/^[-_a-z]+$/', $fotd_location)){
						$params['state-national'] = $fotd_location;
					}
                    $posts = query_posts($params);
					//echo $GLOBALS['wp_query']->request;
                    ?>

                    <?php
                    $odd = 0;
					if (count($posts) > 0){
						while (have_posts()) : the_post();

							$categories = wp_get_post_categories(get_the_ID());
							$states = wp_get_post_terms(get_the_ID(), 'state-national');
							if (count($categories) > 0) {
								$categories = get_the_category_by_ID($categories[0]);
							}
							$url = get_permalink(get_the_ID());//stripslashes_deep(get_post_meta(get_the_ID(), 'url', true));
							?>
						<tr <?php if ($odd++ % 2 == 0) echo 'class="even"'; else echo 'class="odd"';  ?>>
							<td class="date">
								<div class="datebox" style="position:relative;">
									<span class="day day-<?php the_time('d'); ?>"></span>
									<span class="month month-<?php echo strtolower(get_the_time('M')); ?>"></span>
									<span class="year year-<?php the_time('Y'); ?>"></span>
								</div>
							</td>
							<td class="title"><a href="<?php
								if (empty($url)) {
									the_permalink();
								} else {
									echo $url;
								}
								?>"><?php the_title(); ?></a></td>
							<td class="category"><?php echo $categories; ?></td>
							<td class="state-national"><?php echo $states[0]->name; ?></td>
						</tr>
							<?php
						endwhile;
					} else { ?>
                    	<tr>
                        	<td colspan="4">There are no fraud articles for the month, category and location combination  you selected. Please choose a different month, category and location.” </td>
                        </tr>

					<?php } ?>
                    </tbody>
                </table>

                <?php lnwptheme_content_nav('nav-below'); wp_reset_query(); ?>


            </div>
        </article>

    </div>
    <!-- #content -->
</div><!-- #primary -->

<?php get_sidebar('blog'); ?>

<script>
var $j = jQuery.noConflict();

$j(document).ready(function(){

	$j('#fotd-filter-btn').click(function(){

		var keyArr = ['fotd_month', 'fotd_category', 'fotd_location'];

		if (window.location.href.indexOf('?') >  -1){
			var qs = window.location.href.split(/\?(.*)?/);
			var qsCount = qs.length;
			//alert('count: ' + qsCount);

			if (qsCount > 1){
				qs = qs[1];
				//alert(qs);
			} else if (qsCount==1) {
				qs = qs.join();
				//alert('count: ' + qs);
				qs = qs.split("");
				var strCount = qs.length;
				qs = window.location.href.substring(strCount+1, window.location.href.length);
				//alert(qs);
			}

			var qsArr = (qs.indexOf('&') >  -1) ? qs.split('&') : null;
			//alert('here: ' + qsArr);
		} else {
			var qsArr = null;
		}

		var finalQs = '';
		var i = 0;
		if (qsArr!==null){
			$j.each(qsArr, function(k, v){
				var arr = v.split('=');
				var inArray = $j.inArray(arr[0], keyArr);
				if (inArray < 0){
					finalQs += (i > 0 ? '&' : '') + arr.join('=');
					i++;
				}
			});
		}

		var month = $j('#fotd_month').val();
		var category = $j('#fotd_category').val();
		var location = $j('#fotd_location').val();

		if (month!==null && month > 0){
			finalQs += (i > 0 ? '&' : '') + 'fotd_month=' + month; i++;
		}
		if (category!==null && category > 0){
			finalQs += (i > 0 ? '&' : '') + 'fotd_category=' + category; i++;
		}
		if (location!==null && location!=='' && location.match(/[-_a-zA-Z]+/)){
			finalQs += (i > 0 ? '&' : '') + 'fotd_location=' + location; i++;
		}
		//alert('final: ' + finalQs);
		$j(this).attr('disabled', true);

		var goto = window.location.protocol + '//' + window.location.hostname + window.location.pathname.replace(/page+(\/)+[0-9]+(\/)/, '') + (finalQs.match(/^[-_\w\d=&]+$/) ? '?' : '') + finalQs;
		window.location = goto;
	});
});
</script>

<?php get_footer(); ?>
