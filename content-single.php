<?php
/**
 * The template for displaying content in the single.php template
 *
 * @package WordPress
 * @subpackage LexisNexis_WP_Theme
 * @since LexisNexis WP Theme 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <div class="featured-image"><?php if (has_post_thumbnail()) { the_post_thumbnail('post-thumbnail-small'); } ?></div>

    <div class="entry-content">
        <?php the_content(); ?>
        <?php wp_link_pages(array('before' => '<div class="page-link"><span>' . __('Pages:', 'lnwptheme') . '</span>', 'after' => '</div>')); ?>
    </div>
    <!-- .entry-content -->

    <footer class="entry-meta">
		<?php
        $categories_list = get_the_category_list(__(', ', 'lnwptheme'));
        $tag_list = get_the_tag_list('', __(', ', 'lnwptheme'));
        ?>

        <?php if (!empty($categories_list)) { ?>
            <div class="categories" style="display:none;">
                Posted in <?php echo $categories_list; ?>
            </div>
        <?php } ?>

        <?php if (!empty($tag_list)) { ?>
            <div class="tags">
                Tags: <?php echo $tag_list; ?>
            </div>
        <?php } ?>

        <?php edit_post_link(__('Edit', 'lnwptheme'), '<span class="edit-link">', '</span>'); ?>
    </footer>
    <!-- .entry-meta -->

</article>
<!-- #post-<?php the_ID(); ?> -->
