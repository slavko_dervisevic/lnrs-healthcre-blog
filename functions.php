<?php
/**
 * LexisNexis WP Theme functions and definitions
 *
 * Sets up the theme and provides some helper functions. Some helper functions
 * are used in the theme as custom template tags. Others are attached to action and
 * filter hooks in WordPress to change core functionality.
 *
 * The first function, lnwptheme_setup(), sets up the theme by registering support
 * for various features in WordPress, such as post thumbnails, navigation menus, and the like.
 *
 * When using a child theme (see http://codex.wordpress.org/Theme_Development and
 * http://codex.wordpress.org/Child_Themes), you can override certain functions
 * (those wrapped in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before the parent
 * theme's file, so the child theme functions would be used.
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are instead attached
 * to a filter or action hook. The hook can be removed by using remove_action() or
 * remove_filter() and you can attach your own function to the hook.
 *
 * We can remove the parent theme's hook only after it is attached, which means we need to
 * wait until setting up the child theme:
 *
 * <code>
 * add_action( 'after_setup_theme', 'my_child_theme_setup' );
 * function my_child_theme_setup() {
 *     // We are providing our own filter for excerpt_length (or using the unfiltered value)
 *     remove_filter( 'excerpt_length', 'lnwptheme_excerpt_length' );
 *     ...
 * }
 * </code>
 *
 * For more information on hooks, actions, and filters, see http://codex.wordpress.org/Plugin_API.
 *
 * @package WordPress
 * @subpackage LexisNexis_WP_Theme
 * @since LexisNexis WP Theme 1.0
 */

// Ensure the session is started before anything else.
if (!session_id()) session_start();

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if (!isset($content_width))
    $content_width = 584;

/**
 * Tell WordPress to run lnwptheme_setup() when the 'after_setup_theme' hook is run.
 */
add_action('after_setup_theme', 'lnwptheme_setup');

if (!function_exists('lnwptheme_setup')
):
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which runs
     * before the init hook. The init hook is too late for some features, such as indicating
     * support post thumbnails.
     *
     * To override lnwptheme_setup() in a child theme, add your own lnwptheme_setup to your child theme's
     * functions.php file.
     *
     * @uses load_theme_textdomain() For translation/localization support.
     * @uses add_editor_style() To style the visual editor.
     * @uses add_theme_support() To add support for post thumbnails, automatic feed links, and Post Formats.
     * @uses register_nav_menus() To add support for navigation menus.
     * @uses add_custom_background() To add support for a custom background.
     * @uses add_custom_image_header() To add support for a custom header.
     * @uses register_default_headers() To register the default custom header images provided with the theme.
     * @uses set_post_thumbnail_size() To set a custom post thumbnail size.
     *
     * @since LexisNexis WP Theme 1.0
     */ function lnwptheme_setup()
{

    // This theme styles the visual editor with editor-style.css to match the theme style.
    add_editor_style();

    // Add default posts and comments RSS feed links to <head>.
    add_theme_support('automatic-feed-links');

    // This theme uses wp_nav_menu() in one location.
    register_nav_menu('primary', __('Primary Menu', 'lnwptheme'));
    register_nav_menu('footer', __('Footer Menu', 'lnwptheme'));
    register_nav_menu('page-sidebar-menu', 'Page Sidebar Menu');

    // Add support for custom backgrounds
    // add_custom_background();
    add_theme_support('custom-background');

    // This theme uses Featured Images (also known as post thumbnails) for per-post/per-page Custom Header images
    add_theme_support('post-thumbnails');

    // Can't do this, breaks the (abuse) of featured post thumbnails in full
    // size elsewhere in the site.
    // set_post_thumbnail_size(272, 169);

    // Additional image sizes:
    add_image_size('post-thumbnail-small', 272, 169);
    add_image_size('post-thumbnail-smaller', 235, 143);
}
endif; // lnwptheme_setup

if (!function_exists('lnwptheme_header_style')
) :
    /**
     * Styles the header image and text displayed on the blog
     *
     * @since LexisNexis WP Theme 1.0
     */ function lnwptheme_header_style()
{

    // If no custom options for text are set, let's bail
    // get_header_textcolor() options: HEADER_TEXTCOLOR is default, hide text (returns 'blank') or any hex value
    if (HEADER_TEXTCOLOR == get_header_textcolor())
        return;
    // If we get this far, we have custom styles. Let's do this.
    ?>
<style type="text/css">
        <?php
        // Has the text been hidden?
        if ('blank' == get_header_textcolor()) :
            ?>
        #site-title,
        #site-description {
            position: absolute !important;
            clip: rect(1px 1px 1px 1px); /* IE6, IE7 */
            clip: rect(1px, 1px, 1px, 1px);
        }
            <?php
            // If the user has set a custom color for the text use that
        else :
            ?>
        #site-title a,
        #site-description {
            color: #<?php echo get_header_textcolor(); ?> !important;
        }
            <?php endif; ?>
</style>
<?php

}
endif; // lnwptheme_header_style

if (!function_exists('lnwptheme_admin_header_style')
) :
    /**
     * Styles the header image displayed on the Appearance > Header admin panel.
     *
     * Referenced via add_custom_image_header() in lnwptheme_setup().
     *
     * @since LexisNexis WP Theme 1.0
     */ function lnwptheme_admin_header_style()
{
    ?>
<style type="text/css">
    .appearance_page_custom-header #headimg {
        border: none;
    }

    #headimg h1,
    #desc {
        font-family: "Helvetica Neue", Arial, Helvetica, "Nimbus Sans L", sans-serif;
    }

    #headimg h1 {
        margin: 0;
    }

    #headimg h1 a {
        font-size: 32px;
        line-height: 36px;
        text-decoration: none;
    }

    #desc {
        font-size: 14px;
        line-height: 23px;
        padding: 0 0 3em;
    }
        <?php
        // If the user has set a custom color for the text use that
        if (get_header_textcolor() != HEADER_TEXTCOLOR) :
            ?>
        #site-title a,
        #site-description {
            color: #<?php echo get_header_textcolor(); ?>;
        }
            <?php endif; ?>
    #headimg img {
        max-width: 1000px;
        height: auto;
        width: 100%;
    }
</style>
<?php

}
endif; // lnwptheme_admin_header_style

if (!function_exists('lnwptheme_admin_header_image')
) :
    /**
     * Custom header image markup displayed on the Appearance > Header admin panel.
     *
     * Referenced via add_custom_image_header() in lnwptheme_setup().
     *
     * @since LexisNexis WP Theme 1.0
     */ function lnwptheme_admin_header_image()
{
    ?>
<div id="headimg">
    <?php
    if ('blank' == get_theme_mod('header_textcolor', HEADER_TEXTCOLOR) || '' == get_theme_mod('header_textcolor', HEADER_TEXTCOLOR))
        $style = ' style="display:none;"';
    else
        $style = ' style="color:#' . get_theme_mod('header_textcolor', HEADER_TEXTCOLOR) . ';"';
    ?>
    <h1><a id="name"<?php echo $style; ?> onclick="return false;"
           href="<?php echo esc_url(home_url('/')); ?>"><?php bloginfo('name'); ?></a></h1>

    <div id="desc"<?php echo $style; ?>><?php bloginfo('description'); ?></div>
    <?php $header_image = get_header_image();
    if (!empty($header_image)) : ?>
        <img src="<?php echo esc_url($header_image); ?>" alt=""/>
        <?php endif; ?>
</div>
<?php

}
endif; // lnwptheme_admin_header_image

/**
 * Sets the post excerpt length to 40 words.
 *
 * To override this length in a child theme, remove the filter and add your own
 * function tied to the excerpt_length filter hook.
 */
function lnwptheme_excerpt_length($length)
{
    return 40;
}

add_filter('excerpt_length', 'lnwptheme_excerpt_length');

/**
 * Returns a "Continue Reading" link for excerpts
 */
function lnwptheme_continue_reading_link()
{
    //return ' <a href="' . esc_url(get_permalink()) . '">' . __('Continue reading <span class="meta-nav">&rarr;</span>', 'lnwptheme') . '</a>';
    return '';
}

/**
 * Replaces "[...]" (appended to automatically generated excerpts) with an ellipsis and lnwptheme_continue_reading_link().
 *
 * To override this in a child theme, remove the filter and add your own
 * function tied to the excerpt_more filter hook.
 */
function lnwptheme_auto_excerpt_more($more)
{
    return ' &hellip;' . lnwptheme_continue_reading_link();
}

add_filter('excerpt_more', 'lnwptheme_auto_excerpt_more');

/**
 * Adds a pretty "Continue Reading" link to custom post excerpts.
 *
 * To override this link in a child theme, remove the filter and add your own
 * function tied to the get_the_excerpt filter hook.
 */
function lnwptheme_custom_excerpt_more($output)
{
    if (has_excerpt() && !is_attachment()) {
        $output .= lnwptheme_continue_reading_link();
    }
    return $output;
}

add_filter('get_the_excerpt', 'lnwptheme_custom_excerpt_more');

/**
 * Get our wp_nav_menu() fallback, wp_page_menu(), to show a home link.
 */
function lnwptheme_page_menu_args($args)
{
    $args['show_home'] = true;
    return $args;
}

add_filter('wp_page_menu_args', 'lnwptheme_page_menu_args');

/**
 * Register our sidebars and widgetized areas.
 *
 * @since LexisNexis WP Theme 1.0
 */
function lnwptheme_widgets_init() {
  register_sidebar(array(
    'name' => __('Homepage Sidebar', 'ln-ps-wp-theme'),
    'id' => 'homepage-sidebar',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => "</aside>",
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
  ));

  register_sidebar(array(
    'name' => __('All Pages Sidebar', 'ln-ps-wp-theme'),
    'id' => 'sidebar-pages',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => "</aside>",
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
  ));

  register_sidebar(array(
    'name' => __('Single Post (Article) Sidebar', 'ln-ps-wp-theme'),
    'id' => 'sidebar-single',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => "</aside>",
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
  ));
}
add_action('widgets_init', 'lnwptheme_widgets_init');

/**
 * Display navigation to next/previous pages when applicable
 */
function lnwptheme_content_nav($nav_id)
{
    global $wp_query;

    if ($wp_query->max_num_pages > 1) :

        postbar();
        /*
            <nav id="<?php echo $nav_id; ?>">
                <h3 class="assistive-text"><?php _e('Post navigation', 'lnwptheme'); ?></h3>

                <div class="nav-previous"><?php next_posts_link(__('<span class="meta-nav">&larr;</span> Older posts', 'lnwptheme')); ?></div>
                <div class="nav-next"><?php previous_posts_link(__('Newer posts <span class="meta-nav">&rarr;</span>', 'lnwptheme')); ?></div>
            </nav>
        */
    endif;
}

/**
 * Return the URL for the first link found in the post content.
 *
 * @since LexisNexis WP Theme 1.0
 * @return string|bool URL or false when no link is present.
 */
function lnwptheme_url_grabber()
{
    if (!preg_match('/<a\s[^>]*?href=[\'"](.+?)[\'"]/is', get_the_content(), $matches))
        return false;

    return esc_url_raw($matches[1]);
}

/**
 * Count the number of footer sidebars to enable dynamic classes for the footer
 */
function lnwptheme_footer_sidebar_class()
{
    $count = 0;

    if (is_active_sidebar('sidebar-3'))
        $count++;

    if (is_active_sidebar('sidebar-4'))
        $count++;

    if (is_active_sidebar('sidebar-5'))
        $count++;

    $class = '';

    switch ($count) {
        case '1':
            $class = 'one';
            break;
        case '2':
            $class = 'two';
            break;
        case '3':
            $class = 'three';
            break;
    }

    if ($class)
        echo 'class="' . $class . '"';
}

if (!function_exists('lnwptheme_comment')
) :
    /**
     * Template for comments and pingbacks.
     *
     * To override this walker in a child theme without modifying the comments template
     * simply create your own lnwptheme_comment(), and that function will be used instead.
     *
     * Used as a callback by wp_list_comments() for displaying the comments.
     *
     * @since LexisNexis WP Theme 1.0
     */ function lnwptheme_comment($comment, $args, $depth)
{
    $GLOBALS['comment'] = $comment;
    switch ($comment->comment_type) :
        case 'pingback' :
        case 'trackback' :
            ?>
  <li class="post pingback">
    <p><?php _e('Pingback:', 'lnwptheme'); ?> <?php comment_author_link(); ?><?php edit_comment_link(__('Edit', 'lnwptheme'), '<span class="edit-link">', '</span>'); ?></p>
                <?php
            break;
        default :
            ?>
                    <li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
                        <article id="comment-<?php comment_ID(); ?>" class="comment">
                            <footer class="comment-meta">
                                <div class="comment-author vcard">
                                    <?php
                                    $avatar_size = 68;
                                    if ('0' != $comment->comment_parent)
                                        $avatar_size = 39;

                                    echo get_avatar($comment, $avatar_size);

                                    /* translators: 1: comment author, 2: date and time */
                                    printf(__('%1$s on %2$s <span class="says">said:</span>', 'lnwptheme'),
                                        sprintf('<span class="fn">%s</span>', get_comment_author_link()),
                                        sprintf('<a href="%1$s"><time pubdate datetime="%2$s">%3$s</time></a>',
                                            esc_url(get_comment_link($comment->comment_ID)),
                                            get_comment_time('c'),
                                            /* translators: 1: date, 2: time */
                                            sprintf(__('%1$s at %2$s', 'lnwptheme'), get_comment_date(), get_comment_time())
                                        )
                                    );
                                    ?>

                                    <?php edit_comment_link(__('Edit', 'lnwptheme'), '<span class="edit-link">', '</span>'); ?>
                                </div>
                                <!-- .comment-author .vcard -->

                                <?php if ($comment->comment_approved == '0') : ?>
                                <em class="comment-awaiting-moderation"><?php _e('Your comment is in the queue for posting.', 'lnwptheme'); ?></em>
                                <br/>
                                <?php endif; ?>

                            </footer>

                            <div class="comment-content"><?php comment_text(); ?></div>

                            <div class="reply">
                                <?php comment_reply_link(array_merge($args, array('reply_text' => __('Reply <span>&darr;</span>', 'lnwptheme'), 'depth' => $depth, 'max_depth' => $args['max_depth']))); ?>
                            </div>
                            <!-- .reply -->
                        </article>
            <!-- #comment-## -->

                <?php
            break;
    endswitch;
}
endif; // ends check for lnwptheme_comment()

if (!function_exists('lnwptheme_posted_on')
) :
    /**
     * Prints HTML with meta information for the current post-date/time and author.
     * Create your own lnwptheme_posted_on to override in a child theme
     *
     * @since LexisNexis WP Theme 1.0
     */ function lnwptheme_posted_on()
{
    printf(__('<span class="sep">Posted on </span><a href="%1$s" title="%2$s" rel="bookmark"><time class="entry-date" datetime="%3$s" pubdate>%4$s</time></a><span class="by-author"> <span class="sep"> by </span> <span class="author vcard"><a class="url fn n" href="%5$s" title="%6$s" rel="author">%7$s</a></span></span>', 'lnwptheme'),
        esc_url(get_permalink()),
        esc_attr(get_the_time()),
        esc_attr(get_the_date('c')),
        esc_html(get_the_date()),
        esc_url(get_author_posts_url(get_the_author_meta('ID'))),
        sprintf(esc_attr__('View all posts by %s', 'lnwptheme'), get_the_author()),
        esc_html(get_the_author())
    );
}
endif;

/**
 * Adds two classes to the array of body classes.
 * The first is if the site has only had one author with published posts.
 * The second is if a singular post being displayed
 *
 * @since LexisNexis WP Theme 1.0
 */
function lnwptheme_body_classes($classes)
{

    if (!is_multi_author()) {
        $classes[] = 'single-author';
    }

    if (is_singular() && !is_home() && !is_page_template('showcase.php') && !is_page_template('sidebar-page.php'))
        $classes[] = 'singular';

    return $classes;
}

add_filter('body_class', 'lnwptheme_body_classes');

add_action('admin_menu', 'lexisnexisblog_menu');

function lexisnexisblog_menu()
{

    add_menu_page('LexisNexis Blog Settings', 'LexisNexis Blog Settings', 'manage_options', 'lexisnexis-blog-settings',
        'lexisnexisblog_options');

}

function lexisnexisblog_options()
{

    if (!current_user_can('manage_options')) {
        wp_die(__('You do not have sufficient permissions to access this page.'));
    }

    if ($_POST['action'] == 'update-lexisnexisblog-settings') {

        $title = htmlspecialchars_decode($_POST['lexisnexisblog-title']);
        $subtitle = htmlspecialchars_decode($_POST['lexisnexisblog-subtitle']);
        $latest_news_count = esc_attr($_POST['lexisnexisblog-latest-news-count']);
        $youtube_keyword = htmlspecialchars_decode($_POST['lexisnexisblog-youtube-keyword']);
        $scripts_header = $_POST['lexisnexisblog-scripts-header'];
        $scripts_footer = $_POST['lexisnexisblog-scripts-footer'];
        $addthis_username = $_POST['lexisnexisblog-addthis-username'];

        update_option('lexisnexisblog-title', $title);
        update_option('lexisnexisblog-subtitle', $subtitle);
        update_option('lexisnexisblog-latest-news-count', $latest_news_count);
        update_option('lexisnexisblog-youtube-keyword', $youtube_keyword);
        update_option('lexisnexisblog-scripts-header', $scripts_header);
        update_option('lexisnexisblog-scripts-footer', $scripts_footer);
        update_option('lexisnexisblog-addthis-username', $addthis_username);
    }

    ?>
<div class="wrap">
    <div class="icon32" id="icon-options-general"><br></div>
    <h2>LexisNexis Blog Settings</h2>

    <form action="" method="post">
        <input type="hidden" name="action" value="update-lexisnexisblog-settings">
        <table class="form-table">
            <tr valign="top">
                <th scope="row"><label for="lexisnexisblog-title">Header Title</label></th>
                <td><input type="text" class="regular-text"
                           value="<?php echo htmlspecialchars(stripslashes_deep(get_option('lexisnexisblog-title'))); ?>"
                           id="lexisnexisblog-title" name="lexisnexisblog-title" gtbfieldid="1"></td>
            </tr>
            <tr valign="top">
                <th scope="row"><label for="lexisnexisblog-subtitle">Header Sub-Title</label></th>
                <td><input type="text" class="regular-text"
                           value="<?php echo htmlspecialchars(stripslashes_deep(get_option('lexisnexisblog-subtitle'))); ?>"
                           id="lexisnexisblog-subtitle" name="lexisnexisblog-subtitle" gtbfieldid="2"></td>
            </tr>
            <tr valign="top">
                <th scope="row"><label for="lexisnexisblog-latest-news-count">"Upcoming Events" Items Count</label></th>
                <td><input type="text" class="regular-text"
                           value="<?php echo stripslashes_deep(get_option('lexisnexisblog-latest-news-count')); ?>"
                           id="lexisnexisblog-latest-news-count" name="lexisnexisblog-latest-news-count"
                           gtbfieldid="3">
                </td>
            </tr>
            <tr valign="top">
                <th scope="row"><label for="lexisnexisblog-youtube-keyword">YouTube Keyword</label></th>
                <td><input type="text" class="regular-text"
                           value="<?php echo htmlspecialchars(stripslashes_deep(get_option('lexisnexisblog-youtube-keyword', 'debtcollectioninsight.com'))); ?>"
                           id="lexisnexisblog-youtube-keyword" name="lexisnexisblog-youtube-keyword" gtbfieldid="3">
                </td>
            </tr>
            <tr valign="top">
                <th scope="row"><label for="lexisnexisblog-addthis-username">AddThis Username</label></th>
                <td><input type="text" class="regular-text"
                           value="<?php echo htmlspecialchars(stripslashes_deep(get_option('lexisnexisblog-addthis-username', 'lexisnexisins'))); ?>"
                           id="lexisnexisblog-addthis-username" name="lexisnexisblog-addthis-username"
                           gtbfieldid="3"></td>
            </tr>
            <tr valign="top">
                <th scope="row"><label for="lexisnexisblog-scripts-header">Scripts Header</label></th>
                <td>
                    <textarea id="lexisnexisblog-scripts-header" name="lexisnexisblog-scripts-header" rows="10"
                              cols="80"><?php echo htmlspecialchars(stripslashes_deep(get_option('lexisnexisblog-scripts-header'))); ?></textarea>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row"><label for="lexisnexisblog-scripts-footer">Scripts Footer</label></th>
                <td>
                    <textarea id="lexisnexisblog-scripts-footer" name="lexisnexisblog-scripts-footer" rows="10"
                              cols="80"><?php echo htmlspecialchars(stripslashes_deep(get_option('lexisnexisblog-scripts-footer'))); ?></textarea>
                </td>
            </tr>
        </table>
        <p class="submit">
            <input type="submit" value="Save Changes" class="button-primary" name="Submit">
        </p>
    </form>
</div>
    <?php

}

add_action('init', 'lexisnexis_wp_theme_init');

function lexisnexis_wp_theme_init() {

  // Slides, events, content types, resources and resource types are not used on Healthcare:

  /*
  $args = array(
    'labels' => array(
    'name' => _x('Slides', 'post type general name'),
    'singular_name' => _x('Slide', 'post type singular name'),
    'add_new' => _x('Add New', 'slide'),
    'add_new_item' => __('Add New Slide'),
    'edit_item' => __('Edit Slide'),
    'new_item' => __('New Slide'),
    'view_item' => __('View Slide'),
    'search_items' => __('Search Slides'),
    'not_found' => __('No slides found'),
    'not_found_in_trash' => __('No slides found in Trash'),
    'parent_item_colon' => '',
    'menu_name' => 'Slides'

  ),
      'publicly_queryable' => false,
      'exclude_from_search' => true,
      'show_ui' => true,
      'show_in_menu' => true,
      'query_var' => true,
      'rewrite' => true,
      'capability_type' => 'post',
      'has_archive' => true,
      'hierarchical' => false,
      'menu_position' => null,
      'supports' => array('title', 'thumbnail', 'custom-fields')
  );
  register_post_type('slide', $args);

  $labels = array(
      'name' => _x('Events', 'post type general name'),
      'singular_name' => _x('Event', 'post type singular name'),
      'add_new' => _x('Add New', 'Event'),
      'add_new_item' => __('Add New Event'),
      'edit_item' => __('Edit Event'),
      'new_item' => __('New Event'),
      'view_item' => __('View Event'),
      'search_items' => __('Search Events'),
      'not_found' => __('No event found'),
      'not_found_in_trash' => __('No event found in Trash'),
      'parent_item_colon' => '',
      'menu_name' => 'Events'

  );
  $args = array(
      'labels' => $labels,
      'publicly_queryable' => true,
      'exclude_from_search' => true,
      'show_ui' => true,
      'show_in_menu' => true,
      'query_var' => true,
      'rewrite' => true,
      'capability_type' => 'post',
      'has_archive' => true,
      'hierarchical' => false,
      'menu_position' => null,
      'supports' => array('title', 'editor', 'thumbnail', 'custom-fields')
  );
  register_post_type('event', $args);

  register_post_type('resource', array(
    'menu_position' => null,
    'public' => false,
    'publicly_queryable' => false,
    'query_var' => false,
    'exclude_from_search' => false,
    'show_in_menu' => true, // Still show in admin!
    'show_ui' => true, // Still show in admin!
    'capability_type' => 'post',
    'has_archive' => false,
    'hierarchical' => false,
    'supports' => array('title', 'editor', 'custom-fields', 'thumbnail'),
    'labels' => array(
      'name' => ucwords('resource'),
      'singular_name' => ucwords('resource'),
      'add_new' => 'Add New',
      'add_new_item' => "Add New " . ucwords('resource'),
      'edit_item' => "Edit " . ucwords('resource'),
      'new_item' => "New " . ucwords('resource'),
      'view_item' => "View " . ucwords('resource'),
      'search_items' => "Search resources",
      'not_found' => 'No ' . strtolower('resources') . ' found',
      'not_found_in_trash' => 'No ' . strtolower('resources') . ' found in Trash',
      'parent_item_colon' => '',
      'menu_name' => 'Resources'
    )
  ));

  register_taxonomy('resource_type', array('resource'), array(
    'hierarchical' => true,
    'labels' => array(
      'name' => _x('Types', 'taxonomy general name'),
      'singular_name' => _x('Type', 'taxonomy singular name'),
      'search_items' => __('Search Type'),
      'all_items' => __('All Types'),
      'parent_item' => __('Parent Type'),
      'parent_item_colon' => __('Parent Type:'),
      'edit_item' => __('Edit Type'),
      'update_item' => __('Update Type'),
      'add_new_item' => __('Add New Type'),
      'new_item_name' => __('New Type Name'),
      'menu_name' => __('Type'),
    ),
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => array('slug' => 'resource_type'),
  ));

  register_taxonomy(
    'content_types',
    array('post'),
    array(
      'labels' => array(
        'name' => __('Content Types', 'content_types'),
        'singular_name' => __('Content Type', 'content_types'),
        'search_items' => __('Search Content Types', 'content_types'),
        'popular_items' => __('Popular Content Types', 'content_types'),
        'all_items' => __('All Content Types', 'content_types'),
        'parent_item' => __('Parent Content Type', 'content_types'),
        'parent_item_colon' => __('Parent Content Type:', 'content_types'),
        'edit_item' => __('Edit Content Type', 'content_types'),
        'update_item' => __('Update Content Type', 'content_types'),
        'add_new_item' => __('Add New Content Type', 'content_types'),
        'new_item_name' => __('New Content Type Name', 'content_types'),
        'menu_name' => __('Content Types', 'content_types'),
      ),
      'hierarchical' => true,
      'query_var' => true,
      'rewrite' => true,
    )
  );
  */

  // No region support for Healthcare
  /*
  register_taxonomy(
    'region',
    array('post'),
    array(
      'labels' => array(
        'name' => __('Regions', 'region'),
        'singular_name' => __('Region', 'region'),
        'search_items' => __('Search Regions', 'region'),
        'popular_items' => __('Popular Regions', 'region'),
        'all_items' => __('All Regions', 'region'),
        'parent_item' => __('Parent Region', 'region'),
        'parent_item_colon' => __('Parent Region:', 'region'),
        'edit_item' => __('Edit Region', 'region'),
        'update_item' => __('Update Region', 'region'),
        'add_new_item' => __('Add New Region', 'region'),
        'new_item_name' => __('New Region Name', 'region'),
        'menu_name' => __('Regions', 'region'),
      ),
      'hierarchical' => true,
      'query_var' => true,
      'rewrite' => true,
    )
  );
  */

}



/**
 * Register custom taxonomies
 */
function setCustomTaxonomy(){
  /*register_taxonomy('notification_users', 'page', array(
    'hierarchical' => true,
    'labels' => array(
      'name' => _x('Users', 'taxonomy general name'),
      'singular_name' => _x('User', 'taxonomy singular name'),
      'search_items' => __('Search Users'),
      'all_items' => __('All Users'),
      'parent_item' => __('Parent User'),
      'parent_item_colon' => __('Parent User:'),
      'edit_item' => __('Edit User'),
      'update_item' => __('Update User'),
      'add_new_item' => __('Add New User'),
      'new_item_name' => __('New User Name'),
      'menu_name' => __('Users'),
    ),
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => array('slug' => 'notification_users'),
  ));*/
}
add_action('init', 'setCustomTaxonomy');



$feeds = array(
    'industry' => array(
        'http://www.insurancejournal.com/rss/news/',
        'http://www.propertycasualty360.com/Agent-Broker?f=rss',
        'http://www.businessinsurance.com/section/rss?feed=blogs05&mime=xml',
        'http://www.businessinsurance.com/section/rss?feed=NEWS'
    ),
    'connect' => array(
        'http://www.lexisnexis.com/about/lnrssfeed/pr.aspx'
        /*,
              'http://law.lexisnexis.com/practiceareas/News-Headlines/BankruptcyLawCenter/rss',
              'http://law.lexisnexis.com/practiceareas/News-Headlines/Real-Estate/rss'*/
    ),
    'press' => array(
        'http://www.lexisnexis.com/risk/rss/rsspress-release.aspx?Id=all'
    )
);

$links = array(
    'www.businessinsurance.com' => 'Business Insurance',
    'businessinsurance.com' => 'Business Insurance',
    'www.propertycasualty360.com' => 'PC360',
    'www.insurancejournal.com' => 'Insurance Journal',
    'www.collectiontechnology.net' => 'CollectionTechnology.net',
    'www.insidearm.com' => 'insideARM.com',
    'www.americanbanker.com' => 'American Banker',
    'www.acainternational.org' => 'ACA International',
    'www.collectionsrecon.com' => 'Collections Recon'
);

function lexisnexis_feed_title($link)
{
    global $links;

    $args = parse_url($link);
    return isset($links[$args['host']]) ? $links[$args['host']] : $link;
}

function lexisnexis_feed_link($link)
{
    global $links;

    $args = parse_url($link);
    return isset($links[$args['host']]) ? 'http://' . $args['host'] . '' : '';
}

function get_news($type)
{
    global $feeds;

    include_once 'libs/simplepie.inc';

    if (!class_exists('SimplePie') || !isset($feeds[$type])) {
        return array();
    }

    $args = wp_upload_dir();
    $cache_dir = $args['basedir'] . '/cache/';
    if (!is_dir($cache_dir)) {
        @mkdir($cache_dir, 0777);
    }

    $feed = new SimplePie();
    $feed->set_feed_url($feeds[$type]);


    if (is_dir($cache_dir)) {
        $feed->enable_cache(false);
        $feed->set_cache_location($cache_dir);
        $feed->set_cache_duration(1800);
    }

    //$feed->enable_order_by_date();
    if ($type == 'press') {
        $feed->set_item_limit(6);
    } else {
        $feed->set_item_limit(2);
    }

    if ($type == 'connect') $feed->set_item_limit(6);

    $feed->init();
    $items = $feed->get_items();

    return $feed;
}


add_action('save_post', 'fotd_save_postdata');

function fotd_save_postdata($post_id)
{

    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
        return;

    if ('post' == $_POST['post_type']) {
        $cat = $_POST['post_category'][1];
        $state = $_POST['tax_input']['state-national'][1];

        $cat = get_the_category_by_ID($cat);
        $state = get_term($state, 'state-national');
        $state = $state->name;

        update_post_meta($post_id, 'category', $cat);
        update_post_meta($post_id, 'state-national', $state);
    }
}

class Most_Commented_Widget extends WP_Widget
{
    function __construct()
    {
        parent::WP_Widget(false, $name = 'Most Commented');
    }

    function widget($args, $instance)
    {
        extract($args);
        $title = apply_filters('widget_title', $instance['title']);
        $show_pass_post = (bool)$instance['show_pass_post'];
        $duration = intval($instance['duration']);
        if (!in_array($duration, array(0, 1, 7, 30, 365)))
            $duration = 0;
        $num_posts = intval($instance['num_posts']);
        if ($num_posts < 1)
            $num_posts = 5;
        $post_type = $instance['post_type'];
        if (!in_array($post_type, array('post', 'page', 'both')))
            $post_type = 'both';
        if (array_key_exists('echo', $instance))
            $echo = $instance['echo'];
        else
            $echo = true;
        if (array_key_exists('before', $instance)) {
            $before = $instance['before'];
            $after = $instance['after'];
        } else {
            $before = '<li>';
            $after = '</li>';
        }

        global $wpdb;

        if (!$output = wp_cache_get($widget_id)) {
            $output = '';
            /*$request = "SELECT ID, post_title, comment_count FROM $wpdb->posts WHERE comment_count > 0 AND post_status = 'publish'";
            if (!$show_pass_post)
                $request .= " AND post_password = ''";
            if ('both' != $post_type)
                $request .= " AND post_type = '$post_type'";
            if ($duration > 0)
                $request .= " AND DATE_SUB(CURDATE(), INTERVAL $duration DAY) < post_date";
            $request .= " ORDER BY comment_count DESC LIMIT $num_posts";

            $posts = $wpdb->get_results($request);*/

//            if ($echo) {
                $output = '';

//                if (!empty($posts)) {
            query_posts(array('orderby' => 'meta_value', 'meta_key' => 'hot-topics-weight', 'order' => 'ASC'));
                    global $post;
                    while (have_posts()) {
                        the_post();
                        $post_title = get_the_title();
                        $url = get_permalink($post->ID);//stripslashes_deep(get_post_meta($post->ID, 'url', true));
                        $output .= '<div class="hot-topics-item"><a href="' . (empty($url) ? get_permalink($post->ID) : $url) . '" title="' . esc_attr($post_title) . '">' . $post_title . '</a>';
                        $categories = wp_get_post_categories(get_the_ID());
                        $states = wp_get_post_terms(get_the_ID(), 'state-national');
                        if (count($categories) > 0) {
                            $categories = get_the_category_by_ID($categories[0]);
                        }
                        $output .= '<div class="category-state">' . $categories . (count($states) > 0 ? ' / ' . $states[0]->name : '') . '</div>';
                        $output .= '<span class="date">' . get_the_time('F d, Y') . '</span></div>';
                    }
            wp_reset_query();
                /*} else {
                    $output .= $before . 'None found' . $after;
                }*/

                if (!array_key_exists('not_widget', $instance)) {
                    if ($title)
                        $title = $before_title . $title . $after_title;

                    $output = $before_widget . $title . '<ul>' . $output . '</ul>' . $after_widget;
                }
            /*} else {
                $output = $posts;
            }*/

            wp_cache_set($widget_id, $output, '', 1800);
        }

        if ($echo)
            echo $output;
        else
            return $output;
    }

    function update($new_instance, $old_instance)
    {
        $new_instance['show_pass_post'] = isset($new_instance['show_pass_post']);

        wp_cache_delete($this->id);

        return $new_instance;
    }

    function form($instance)
    {
        $title = esc_attr($instance['title']);
        $show_pass_post = (bool)$instance['show_pass_post'];
        $duration = intval($instance['duration']);
        if (!in_array($duration, array(0, 1, 7, 30, 365)))
            $duration = 0;
        $num_posts = intval($instance['num_posts']);
        if ($num_posts < 1)
            $num_posts = 5;
        $post_type = $instance['post_type'];
        if (!in_array($post_type, array('post', 'page', 'both')))
            $post_type = 'both';
        ?>
    <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?> <input class="widefat"
                                                                                              id="<?php echo $this->get_field_id('title'); ?>"
                                                                                              name="<?php echo $this->get_field_name('title'); ?>"
                                                                                              type="text"
                                                                                              value="<?php echo $title; ?>"/></label>
    </p>
    <p><label for="<?php echo $this->get_field_id('post_type'); ?>"><?php _e('Display:'); ?>
        <select id="<?php echo $this->get_field_id('post_type'); ?>"
                name="<?php echo $this->get_field_name('post_type'); ?>">
            <?php
            $post_type_choices = array('post' => __('Posts'), 'page' => __('Pages'), 'both' => __('Posts & Pages'));
            foreach ($post_type_choices as $post_type_value => $post_type_text) {
                echo "<option value='$post_type_value' " . ($post_type == $post_type_value ? "selected='selected'" : '') . ">$post_type_text</option>\n";
            }
            ?>
        </select>
    </label></p>
    <p><label for="<?php echo $this->get_field_id('num_posts'); ?>"><?php _e('Maximum number of results:'); ?>
        <select id="<?php echo $this->get_field_id('num_posts'); ?>"
                name="<?php echo $this->get_field_name('num_posts'); ?>">
            <?php
            for ($i = 1; $i <= 20; ++$i) {
                echo "<option value='$i' " . ($num_posts == $i ? "selected='selected'" : '') . ">$i</option>\n";
            }
            ?>
        </select>
    </label></p>
    <p><label for="<?php echo $this->get_field_id('duration'); ?>"><?php _e('Limit to:'); ?>
        <select id="<?php echo $this->get_field_id('duration'); ?>"
                name="<?php echo $this->get_field_name('duration'); ?>">
            <?php
            $duration_choices = array(1 => __('1 Day'), 7 => __('7 Days'), 30 => __('30 Days'), 365 => __('365 Days'), 0 => __('All Time'));
            foreach ($duration_choices as $duration_num => $duration_text) {
                echo "<option value='$duration_num' " . ($duration == $duration_num ? "selected='selected'" : '') . ">$duration_text</option>\n";
            }
            ?>
        </select>
    </label></p>
    <p><label for="<?php echo $this->get_field_id('show_pass_post'); ?>"><input
        id="<?php echo $this->get_field_id('show_pass_post'); ?>" class="checkbox" type="checkbox"
        name="<?php echo $this->get_field_name('show_pass_post'); ?>"<?php echo checked($show_pass_post); ?> /> <?php _e('Include password protected posts/pages'); ?>
    </label></p>
    <?php
    }

}

add_action('widgets_init', create_function('', 'return register_widget( "Most_Commented_Widget" );'));

if (!function_exists('mdv_most_commented')) {
    function mdv_most_commented($num_posts = 5, $before = '<li>', $after = '</li>', $show_pass_post = false, $duration = 0, $echo = true, $post_type = 'both')
    {
        $options = array(
            'num_posts' => $num_posts,
            'before' => $before,
            'after' => $after,
            'show_pass_post' => $show_pass_post,
            'duration' => $duration,
            'echo' => $echo,
            'post_type' => $post_type,
            'not_widget' => true
        );
        $args = array('widget_id' => 'most_commented_widget_' . md5(var_export($options, true)));
        $most_commented = new Most_Commented_Widget();

        if ($echo)
            $most_commented->widget($args, $options);
        else
            return $most_commented->widget($args, $options);
    }
}

function the_expert($args=array()) {
  echo get_the_expert($args);
}

function get_the_expert($args=array()) {
  $defaults = Array(
    'show_title' => true,
    'show_link' => true,
    'show_thumbnail' => false,
    'id' => false,
    'prefix' => "Written by: ",
    'show_prefix' => true
  );
  $args = array_merge($defaults, $args);
  $s = "";

  if ($args['id'] === false) {
    $args['id'] = $post->ID;
  }

  // Fetch the ACF field "author" (the full name of the author from the Experts
  // repeater) from the provided post ID:
  $article_author_name = get_field('author', $args['id']);

  // We need to loop through that options repeater until we find a matching
  // expert record.
  while (have_rows('experts', 'option')) {
    the_row();
    $name = get_sub_field('name');
    if ($name == $article_author_name) {

      if ($args['show_thumbnail']) {
        $thumbnail = get_sub_field('thumbnail', $userkey);
        if ($thumbnail) {
          $s .= '<img src="' . esc_url($thumbnail['url']) . ' alt="' . esc_attr($name) . '" class="size-thumbnail" />';
        }
      }

      $author_name_and_title = $name;
      if ($args['show_title']) {
        $author_title = get_sub_field('job_title');
        if ($author_title) $author_name_and_title .= ', ' . $author_title;
      }

      $author_page = get_field('our_experts_page', 'options');

      if ($args['show_prefix']) {
        $s .= $args['prefix'];
      }

      if ($args['show_link'] && $author_page && get_sub_field('visible')) {
        // Author page must be specified and the author visible on it:
        $s .= '<a href="' . esc_url($author_page . '#' . urlencode($name)) . '" rel="author">' . $author_name_and_title . '</a>';
      }
      else {
        // This user is not flagged (in Users administration) to be displayed on the Experts page.  Or there is no Experts page:
        $s .= $author_name_and_title;
      }
    }
  }
  return $s;
}

// Process shortcodes within all widgets.
add_filter('widget_text', 'do_shortcode');

// Add responsive container to embeds (e.g. YouTube links in the editor)
function handle_embed_html($html) {
  return '<div class="oembed-container">' . $html . '</div>';
}
add_filter('embed_oembed_html', 'handle_embed_html', 10, 3);
add_filter('video_embed_html', 'handle_embed_html');

// ACF options pages (more than one!)
if( function_exists('acf_add_options_page') ) {
  // ACF4 ALWAYS add an default options page if the module is on
  // Since I gotta use it anyway, let's declare it if we ever go to ACF5
  acf_add_options_page();
  acf_add_options_page('Our Experts');
}

// Set any select field named 'author' to pull its values from the 'experts'
// list in the options page:
add_filter('acf/load_field/name=author', function($field) {
  $field['choices'] = array();
  while(have_rows('experts', 'option')) {
    the_row();
    $name = get_sub_field('name');
    // Save full name as the value; I considered making this the sub_field
    // object key, but things will act weird if, say, they delete and
    // recreate the expert.  You just can't have experts with the exact
    // same name, or we might point to the wrong one.  Should be fine.
    $field['choices'][$name] = $name;
  }
  return $field;
});
