<?php
/**
 * The template for displaying posts in the Status Post Format on index and archive pages
 *
 * Learn more: http://codex.wordpress.org/Post_Formats
 *
 * @package WordPress
 * @subpackage LexisNexis_WP_Theme
 */
?>

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<header class="entry-header">
			<hgroup>
				<h2 class="entry-title"><a href="<?php echo get_post_meta(get_the_ID(), 'url', true); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'lnwptheme' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
			</hgroup>
		</header><!-- .entry-header -->
	</article><!-- #post-<?php the_ID(); ?> -->
