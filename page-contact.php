<?php
/**
 * Template Name: Contact Us
 */

get_header(); ?>

<div class="breadcrumb-stripe">
  <div class="container">
  	<?php if(function_exists('simple_breadcrumb')) {simple_breadcrumb();} ?>
  </div>
</div>

<div class="container">
<div class="row">
<div id="primary" class="col-md-9">

			<div id="content" role="main">

				<?php the_post(); ?>

				<?php get_template_part( 'content', 'page' ); ?>

				<div class="contact-form">
                    <?php include_once 'contactform/contact-form.html'; ?>
                </div>

				<?php //comments_template( '', true ); ?>

			</div><!-- #content -->

		</div><!-- #primary -->

<?php get_sidebar(); ?>
</div> <!-- row -->
</div> <!-- container -->
<?php get_footer(); ?>
