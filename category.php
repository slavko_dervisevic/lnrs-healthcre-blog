<?php
/**
 * The template for displaying Category Archive pages.
 * Template Name: Archive List
 *
 * @package WordPress
 * @subpackage LexisNexis_WP_Theme
 * @since LexisNexis WP Theme 1.0
 */

get_header(); ?>

<style>
#main{
	background:none;
}
</style>

<div class="row">
<div id="primary" class="col-md-12">

			<div id="content" role="main" style="width:89%;">

			<?php if(function_exists('simple_breadcrumb')) {simple_breadcrumb();} ?>

			<?php if ( have_posts() ) : ?>

				<header class="page-header">
					<h1 class="page-title category">
						<?php printf( __( '%s', 'lnwptheme' ), '<span>' . single_cat_title( '', false ) . '</span>' ); ?>
                    </h1>

                    <div class="category-archive-meta">
						<p><?php $desc = category_description($v->cat_ID);
                        preg_match('/\[long\](.*)\[\/long\]/', $desc, $matches);
                        $desc = preg_replace('/\[long\](.*)\[\/long\]/', '$1', $matches[0]);
                        echo $desc;
                        ?></p>
                    </div>
				</header>

				<?php //lnwptheme_content_nav( 'nav-above' ); ?>

				<?php /* Start the Loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>
					<?php
						/* Include the Post-Format-specific template for the content.
						 * If you want to overload this in a child theme then include a file
						 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
						 */
						get_template_part( 'content', 'category-post-excerpt' );
					?>
				<?php endwhile; ?>

			<?php else : ?>

				<article id="post-0" class="post no-results not-found">
					<header class="entry-header">
						<h1 class="entry-title"><?php _e( 'Nothing Found', 'lnwptheme' ); ?></h1>
					</header><!-- .entry-header -->

					<div class="entry-content">
						<p><?php _e( 'Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'lnwptheme' ); ?></p>
						<?php get_search_form(); ?>
					</div><!-- .entry-content -->
				</article><!-- #post-0 -->

			<?php endif; ?>

			</div><!-- #content -->
		</div><!-- #primary -->
</div> <!-- row -->
<?php //get_sidebar(); ?>
<?php get_footer(); ?>
