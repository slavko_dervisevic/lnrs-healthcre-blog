<?php
/**
 * Template Name: Archive List
 *
 * @package WordPress
 * @subpackage LexisNexis_WP_Theme
 */
global $wpdb, $wp_locale;

get_header();
?>

<style>
#fotd_category{
  margin:0 5px 14px 0;
}
</style>


<div class="header-stripe">
  <div class="container">
    <header class="entry-header">
      <h1 class="entry-title"><?php the_title(); ?></h1>
    </header>
  </div>
</div>

<div class="breadcrumb-stripe">
  <div class="container">
  	<?php if(function_exists('simple_breadcrumb')) {simple_breadcrumb();} ?>
  </div>
</div>
<div class="filters-stripe">
  <div class="container">

                <?php

      if (empty($_SESSION['fotd_sort'])) {
                    $_SESSION['fotd_sort'] = 'DESC';
                }

                if (empty($_SESSION['fotd_sortby'])) {
                    $_SESSION['fotd_sortby'] = 'date';
                }

        if ($_GET['fotd_sortby'] != NULL && $_SESSION['fotd_sortby'] != $_GET['fotd_sortby']) {
          if ($_GET['fotd_sortby'] == 'date') {
            $_GET['fotd_sort'] = 'DESC';
          } else {
            $_GET['fotd_sort'] = 'ASC';
          }
        }
                if (esc_attr($_GET['fotd_sort'])) {
                    $_SESSION['fotd_sort'] = $sort = esc_attr($_GET['fotd_sort']);
                } else {
                    $sort = $_SESSION['fotd_sort'];
                }

                if (esc_attr($_GET['fotd_sortby'])) {
                    $_SESSION['fotd_sortby'] = $sortby = esc_attr($_GET['fotd_sortby']);
                } else {
                    $sortby = $_SESSION['fotd_sortby'];
                }

        $_SESSION['fotd_month'] = $fotd_month = preg_replace('/[^0-9]+/', '', (isset($_GET['fotd_month']) ? $_GET['fotd_month'] : NULL));
        $_SESSION['fotd_category'] = $fotd_category = preg_replace('/[^0-9]+/', '', (isset($_GET['fotd_category'])) ? $_GET['fotd_category'] : NULL);
        $_SESSION['fotd_location'] = $fotd_location = preg_replace('/[^-_a-zA-Z]+/', '', (isset($_GET['fotd_location'])) ? $_GET['fotd_location'] : NULL);

        // Uses IDs (or blank) and can thereby be filtered as numeric:
        $_SESSION['archive_post_tag'] = $archive_post_tag = preg_replace('/[^0-9]+/', '', (isset($_GET['archive_post_tag'])) ? $_GET['archive_post_tag'] : NULL);

        // Uses a complete string:
        $_SESSION['archive_author'] = $archive_author = (isset($_GET['archive_author'])) ? $_GET['archive_author'] : NULL;

                ?>
<!--
                <div style="margin-bottom:5px;">Select date, category, tag and author to narrow the archive. You can select one or all options.</div>
-->

                <!--Start month drop down menu-->
                <?php
          $query = "
            SELECT DISTINCT YEAR( post_date ) AS year, MONTH( post_date ) AS month
            FROM $wpdb->posts
            WHERE post_type = 'post'
            ORDER BY post_date DESC";
          // echo $query;
          $months = $wpdb->get_results($query);
          $month_count = count( $months );

          if ( !$month_count || ( 1 == $month_count && 0 == $months[0]->month )) {return;}

          $m = isset( $_GET['m'] ) ? (int) $_GET['m'] : 0;
        ?>
        <div class="filters-box">
        <div class="filter-box">
          FILTER BY DATE
                    <select id='fotd_month' >
                        <option<?php selected( $m, 0 ); ?> value='0'><?php _e('All Dates'); ?></option>
        <?php
          foreach ( $months as $arc_row ) {
            if ( 0 == $arc_row->year ){continue;}

            $month = zeroise( $arc_row->month, 2 );
            $year = $arc_row->year;
            printf("<option %s value='%s' " . ($fotd_month==esc_attr($arc_row->year . $month) ? 'selected="selected"' : '') . ">%s</option>\n",
              selected( $m, $year . $month, false ),
              esc_attr( $arc_row->year . $month ),
              ucwords( $wp_locale->get_month( $month )) . " $year"
            );
          }
        ?>
          </select>
        </div><!-- filter box-->
        <!--End month drop down menu-->

        <div class="filter-box">
          FILTER BY CATEGORY
        <!--Start category drop down menu-->
        <?php echo wp_dropdown_categories(array(
            'show_option_all' => __('All Categories'),
            'show_option_none' => '',
            'orderby' => 'name',
            'order' => 'ASC',
            'show_last_update' => 0,
            'show_count' => 0,
            'hide_empty' => 1,
            'exclude' => '',
            'echo' => 0,
            'selected' => $fotd_category,
            'hierarchical' => 0,
            'name' => 'fotd_category',
            'id' => 'fotd_category',
            'class' => 'postform',
            'depth' => 0,
            'tab_index' => 0,
            'taxonomy' => 'category',
            'hide_if_empty' => true
          ));
        ?>
        </div><!-- filter box -->
                <style>select#fotd_category{width:220px;}</style>
        <!--End category drop down menu-->

                <br />

              <div class="filter-box">
                FILTER BY AUTHOR
                <select id="archive_author">
                  <option value="">All Authors</option>
<?php
while (have_rows('experts', 'option')):
  the_row();
  // On this page, show ALL ACF authors, not just ones marked 'visible'
  // if (!get_sub_field('visible')) continue;
  $name = get_sub_field('name');
?>
                  <option value="<?php echo $name; ?>"<?php selected($archive_author, $name); ?>><?php echo $name; ?></option>
<?php endwhile; ?>
                </select>
          </div>

          <div class="filter-box">
            FILTER BY CONTENT
                <select id="archive_post_tag">
                  <option value="">All Tags</option>
<?php
$terms = get_categories(array(
  'taxonomy' => 'post_tag'
));
foreach($terms as $term):
?>
                  <option value="<?php echo $term->term_id; ?>"<?php selected($archive_post_tag, $term->term_id); ?>><?php echo $term->name; ?></option>
<?php endforeach; ?>
                </select>
            </div>
      </div>
      <div class="filters-box">
            <div class="filter-box submit">
                <input type="button" id="fotd-filter-btn" class="button-secondary" value="Refresh"/>
            </div>
      </div>

</div><!-- container -->
</div><!-- filters-stripe -->

<div class="container">
<div class="row">
<div id="primary" class="col-md-9">
<!-- <div id="primary"> -->

    <div id="content" role="main">

        <article style="border: none !important;" id="post-<?php the_post(); the_ID(); ?>" <?php post_class(); ?>>
            <!-- .entry-header -->

            <div class="entry-content" style="padding-top:15px;">


                <?php the_content(); ?>

                <table class="posts-table" cellspacing="0" style="margin:-10px 0 0 0;">
                    <thead>
                    <tr>
                        <th>
                            <a <?php if ($sort == 'DESC' && $sortby == 'date') echo ' class="sort-down" '; else if ($sort == 'ASC' && $sortby == 'date') echo ' class="sort-up" '  ?>
                                href="?fotd_sort=<?php if ($sort == 'DESC') echo 'ASC'; else if ($sort == 'ASC') echo 'DESC'; ?>&fotd_sortby=date<?php echo (!is_null($fotd_month) ? '&fotd_month=' . $fotd_month : ''); ?><?php echo (!is_null($fotd_category) ? '&fotd_category=' . $fotd_category : ''); ?><?php echo (!is_null($fotd_location) ? '&fotd_location=' . $fotd_location : ''); ?><?php echo (!is_null($archive_author) ? '&archive_author=' . $archive_author : ''); ?><?php echo (!is_null($archive_post_tag) ? '&archive_post_tag=' . $archive_post_tag : ''); ?>">Date</a>
                        </th>
                        <th>
              <a <?php if ($sort == 'DESC' && $sortby == 'headline') echo ' class="sort-down" '; else if ($sort == 'ASC' && $sortby == 'headline') echo ' class="sort-up" '  ?>
                                href="?fotd_sort=<?php if ($sort == 'DESC') echo 'ASC'; else if ($sort == 'ASC') echo 'DESC'; ?>&fotd_sortby=headline<?php echo (!is_null($fotd_month) ? '&fotd_month=' . $fotd_month : ''); ?><?php echo (!is_null($fotd_category) ? '&fotd_category=' . $fotd_category : ''); ?><?php echo (!is_null($fotd_location) ? '&fotd_location=' . $fotd_location : ''); ?><?php echo (!is_null($archive_author) ? '&archive_author=' . $archive_author : ''); ?><?php echo (!is_null($archive_post_tag) ? '&archive_post_tag=' . $archive_post_tag : ''); ?>">Headline</a>
                        </th>
                        <th>
                            <a <?php if ($sort == 'DESC' && $sortby == 'cat') echo ' class="sort-down" '; else if ($sort == 'ASC' && $sortby == 'cat') echo ' class="sort-up" '  ?>
                                href="?fotd_sort=<?php if ($sort == 'DESC') echo 'ASC'; else if ($sort == 'ASC') echo 'DESC'; ?>&fotd_sortby=cat<?php echo (!is_null($fotd_month) ? '&fotd_month=' . $fotd_month : ''); ?><?php echo (!is_null($fotd_category) ? '&fotd_category=' . $fotd_category : ''); ?><?php echo (!is_null($fotd_location) ? '&fotd_location=' . $fotd_location : ''); ?><?php echo (!is_null($archive_author) ? '&archive_author=' . $archive_author : ''); ?><?php echo (!is_null($archive_post_tag) ? '&archive_post_tag=' . $archive_post_tag : ''); ?>">Category</a>
                        </th>
                    </tr>
                    </thead>
                    </table>
                    <div class="archive-list">
                    <?php
                    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                    $params = array(
                      'paged'   => $paged,
                      'order'   => $sort,
                      'orderby' => ($sortby == 'date' ? $sortby : ($sortby == 'headline' ? 'title' : 'meta_value')),
                      'posts_per_page' => 10
                    );
                    if ($sortby == 'cat') {
                        $params['meta_key'] = 'category';
                    }
                    // Date filter:
                    if (preg_match('/^[0-9]{6}+$/', $fotd_month)){
                      $params['year'] = substr($fotd_month, 0, 4);
                      $params['monthnum'] = substr($fotd_month, 4, 2);
                    }
                    // Category filter:
                    if (preg_match('/^[0-9]+$/', $fotd_category)) {
                      $params['category__and'] = $fotd_category;
                    }
                    // Author filter:
                    if (!empty($archive_author)) {
                      // $params['author'] = $archive_author;
                      // Technically combining meta_key/meta_value with meta_query is
                      // superseded in WP 4.2 as per https://make.wordpress.org/core/2015/03/30/query-improvements-in-wp-4-2-orderby-and-meta_query/
                      // but still has legacy support so hey!
                      $params['meta_query'] = array(
                        'relation' => 'AND',
                        array(
                          'key' => 'author',
                          'value' => $archive_author
                        )
                      );
                    }
                    // Tag ('post_tag' taxonomy term) filter:
                    if (preg_match('/^[0-9]+$/', $archive_post_tag)) {
                      $params['tax_query'] = array(array(
                        'taxonomy' => 'post_tag',
                        'field' => 'term_id',
                        'terms' => $archive_post_tag
                      ));
                    }

                    $posts = query_posts($params);
          //echo $GLOBALS['wp_query']->request;
                    ?>

                    <?php
                    $odd = 0;
          if (count($posts) > 0){
            while (have_posts()) : the_post();

              $categories = wp_get_post_categories(get_the_ID());
              $states = wp_get_post_terms(get_the_ID(), 'state-national');
              if (count($categories) > 0) {
                $categories = get_the_category_by_ID($categories[0]);
              }
              $url = get_permalink(get_the_ID());//stripslashes_deep(get_post_meta(get_the_ID(), 'url', true));
              ?>
            <div class="article-item">
              <div class="article-thumb">
              <?php if (has_post_thumbnail()) { the_post_thumbnail('post-thumbnail-small'); } ?>
              </div>
              <div class="article-detail">
                <div class="article-date">
                  <?php echo strtolower(get_the_time('F')); ?>
                  <?php the_time('d'); ?>, <?php the_time('Y'); ?>
                  |
                  <?php echo $categories; ?>
                </div>
                <div class="article-title"><a href="<?php if (empty($url)) { the_permalink(); } else { echo $url; } ?>"><?php the_title(); ?></a></div>
                <div class="article-expert"><?php the_expert(array('show_link' => false)); ?></div>
              </div>
              
            </div>
              <?php
            endwhile;
          } else { ?>
                      <div class="no-articles">
                        There are no articles for the month, category, author and tag options that you selected. Please choose different criteria.
                      </div>

          <?php } ?>
            </div>

            <?php 
            ob_start();
            lnwptheme_content_nav('nav-below'); 
                        
            $page_nav = ob_get_contents();
            ob_end_clean();


            wp_reset_query(); 
            
            ?>

            </div>
        </article>

    </div>
    <!-- #content -->
</div><!-- #primary -->

<?php get_sidebar(); ?>

</div> <!-- row -->
</div> <!-- container -->

<div class="paging-stripe">
  <div class="container"> 
    <div class="row">
      <div class="col-md-9">
                <?php echo $page_nav ?>
<!--test only  -->
<div class="pagebar"><span>&nbsp;</span><span class="inactive"><i class="prev"></i></span>
<span class="this-page">1</span>
<a href="https://qa.blogs.lexisnexis.com/healthcare/archive/page/2/" title="Page 2">2</a>
<a href="https://qa.blogs.lexisnexis.com/healthcare/archive/page/3/" title="Page 3">3</a>
<a href="https://qa.blogs.lexisnexis.com/healthcare/archive/page/4/" title="Page 4">4</a>
<a href="https://qa.blogs.lexisnexis.com/healthcare/archive/page/5/" title="Page 5">5</a>
<a href="https://qa.blogs.lexisnexis.com/healthcare/archive/page/2/" title="Page 2"><i class="next"></i></a>
</div><!--test only  -->
      </div> <!-- col -->
    </div> <!-- row -->
  </div> <!-- container -->
</div> <!-- container -->



<script>
var $j = jQuery.noConflict();

$j(document).ready(function(){

  $j('#fotd-filter-btn').click(function(){

    var keyArr = ['fotd_month', 'fotd_category', 'fotd_location', 'archive_author', 'archive_post_tag'];

    if (window.location.href.indexOf('?') >  -1){
      var qs = window.location.href.split(/\?(.*)?/);
      var qsCount = qs.length;
      //alert('count: ' + qsCount);

      if (qsCount > 1){
        qs = qs[1];
        //alert(qs);
      } else if (qsCount==1) {
        qs = qs.join();
        //alert('count: ' + qs);
        qs = qs.split("");
        var strCount = qs.length;
        qs = window.location.href.substring(strCount+1, window.location.href.length);
        //alert(qs);
      }

      var qsArr = (qs.indexOf('&') >  -1) ? qs.split('&') : null;
      //alert('here: ' + qsArr);
    } else {
      var qsArr = null;
    }

    var finalQs = '';
    var i = 0;
    if (qsArr!==null){
      $j.each(qsArr, function(k, v){
        var arr = v.split('=');
        var inArray = $j.inArray(arr[0], keyArr);
        if (inArray < 0){
          finalQs += (i > 0 ? '&' : '') + arr.join('=');
          i++;
        }
      });
    }

    var month = $j('#fotd_month').val();
    var category = $j('#fotd_category').val();
    var location = $j('#fotd_location').val();

    if (month!==null && month > 0){
      finalQs += (i > 0 ? '&' : '') + 'fotd_month=' + month; i++;
    }
    if (category!==null && category > 0){
      finalQs += (i > 0 ? '&' : '') + 'fotd_category=' + category; i++;
    }

    var archive_author = $j('#archive_author').val();
    if (archive_author) {
      finalQs += (i > 0 ? '&' : '') + 'archive_author=' + archive_author; i++;
    }
    var archive_post_tag = $j('#archive_post_tag').val();
    if (archive_post_tag) {
      finalQs += (i > 0 ? '&' : '') + 'archive_post_tag=' + archive_post_tag; i++;
    }

    //alert('final: ' + finalQs);
    $j(this).attr('disabled', true);

    var goto = window.location.protocol + '//' + window.location.hostname + window.location.pathname.replace(/page+(\/)+[0-9]+(\/)/, '') + (finalQs != '' ? '?' : '') + finalQs;
    window.location = goto;
  });
});
</script>

<?php get_footer(); ?>
