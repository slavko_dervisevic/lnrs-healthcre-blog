
<SCRIPT language="JavaScript">
function getURLParam(strParamName)
{
  var regexS = "[\\?&]"+strParamName+"=([^&#]*)";
  var regex = new RegExp( regexS );
  var tmpURL = window.location.href;
  var results = regex.exec( tmpURL );
  if( results == null )
    return "";
  else
    return results[1];
}
var p_source = getURLParam( 'source' );
</SCRIPT>

<SCRIPT TYPE="text/javascript">
var errorSet = null;
FieldObj = function() {
   var Field;
   this.get_Field = function() { return Field; }
   this.set_Field = function(val) { Field = val; }
   var ErrorMessage;
   this.get_ErrorMessage = function() { return ErrorMessage; }
   this.set_ErrorMessage = function(val) { ErrorMessage = val; }
}
function ResetHighlight() {
   var field;
   if (errorSet != null) {
      for (var i = 0; i < errorSet.length; i++) {
         errorSet[i].Field.className = 'form_text'
      }
    }
   errorSet = new Array();
}
function DisplayErrorSet(ErrorSet) {
   var element;
   var ErrorMessage = '';
   for (var i = 0; i < ErrorSet.length; i++) {
      ErrorMessage = ErrorMessage + ErrorSet[i].ErrorMessage + '\n';
      ErrorSet[i].Field.className = 'elqFieldValidation';
   }
   if (ErrorMessage != '')
      alert(ErrorMessage);
}
function ValidateRequiredField(Element, args) {
   var elementVal=Element.value;
   var testPass=true;
   if (Element) {
      if (args.Type == 'text') {
         if (Element.value == null || Element.value == "") {
            return false;
         }
      }
      else if (args.Type == 'singlesel') {
         if (Element.value == null || Element.value == "") {
            return false;
         }
   }
      else if (args.Type == 'multisel') {
         var selCount=0;
         for (var i=0; i<Element.length; i++) {
              if (Element[i].selected && Element[i].value !='') {
                 selCount += 1;
              }
         }
      if (selCount == 0)
         return false;
   }
   }
   else
      testPass = false;
return testPass;
}
function ValidateEmailAddress(Element) {
   var varRegExp='^[A-Z0-9!#\\$%&\'\\*\\+\\-/=\\?\\^_`\\{\\|\\}~][A-Z0-9!#\\$%&\'\\*\\+\\-/=\\?\\^_`\\{\\|\\}~\\.]{0,62}@([A-Z0-9](?:[A-Z0-9\\-]{0,61}[A-Z0-9])?(\\.[A-Z0-9](?:[A-Z0-9\\-]{0,61}[A-Z0-9])?)+)$';
   if ((Element) && (Element.value != '')) {
      var reg = new RegExp(varRegExp,"i");
      var match = reg.exec(Element.value);
         if ((match) && (match.length=3) && (match[1].length<=255) && ((match[2].length>=3) & (match[2].length<=7)))
            return true;
   }
   return false;

}
function ValidateDataTypeLength(Element, args, ErrorMessage) {
   var elementVal = Element.value;
   var testPass = true;
   if (Element) {
      if (args.Type == 'text') {
         if ((elementVal == '')) {
            testPass = false;
         }
         if ((args.Minimum != '') && (elementVal.length < args.Minimum))
            testPass = false;
         if ((args.Maximum != '') && (elementVal.length > args.Maximum))
            testPass = false;
      }
      else if (args.Type == 'numeric') {
         if ((elementVal == '')) {
            testPass = false;
         }
         if ((elementVal != '') && (elementVal != parseFloat(elementVal)))
            testPass = false;
         if (args.Minimum != '') {
            if ((elementVal == '') || (parseFloat(elementVal) < args.Minimum))
            testPass = false;
         }
         if (args.Maximum != '') {
            if ((elementVal != '') && (parseFloat(elementVal) > args.Maximum))
               testPass = false;
         }
      }
   }
   else
      testPass = false;
   return testPass;
}
function CheckElqForm(elqForm) {
var args = null;
var allValid = true;
if (elqForm == null) {
   alert('Unable to execute form validation!\Unable to locate correct form');
   return false;
}
ResetHighlight();
formField = new FieldObj();
formField.Field = elqForm.elements['FirstName'];
formField.ErrorMessage ='First Name is required.'
args = {'Type': 'text' };
if (formField.Field != null) {
   if (!ValidateRequiredField(formField.Field, args)) {
      errorSet.push(formField);
      allValid = false;
   }
}
formField = new FieldObj();
formField.Field = elqForm.elements['LastName'];
formField.ErrorMessage ='Last Name is required.'
args = {'Type': 'text' };
if (formField.Field != null) {
   if (!ValidateRequiredField(formField.Field, args)) {
      errorSet.push(formField);
      allValid = false;
   }
}
formField = new FieldObj();
formField.Field = elqForm.elements['Company'];
formField.ErrorMessage ='Company is required.'
args = {'Type': 'text' };
if (formField.Field != null) {
   if (!ValidateRequiredField(formField.Field, args)) {
      errorSet.push(formField);
      allValid = false;
   }
}

formField = new FieldObj();
formField.Field = elqForm.elements['Industry'];
formField.ErrorMessage ='Industry is required.'
args = {'Type': 'singlesel' };
if (formField.Field != null) {
   if (!ValidateRequiredField(formField.Field, args)) {
      errorSet.push(formField);
      allValid = false;
   }
}

formField = new FieldObj();
formField.Field = elqForm.elements['Phone'];
formField.ErrorMessage ='Phone is required.'
args = {'Type': 'text' };
if (formField.Field != null) {
   if (!ValidateRequiredField(formField.Field, args)) {
      errorSet.push(formField);
      allValid = false;
   }
}
formField = new FieldObj();
formField.Field = elqForm.elements['EmailAddress'];
formField.ErrorMessage ='Email Address is required and must be valid.'
if (formField.Field != null) {
   if (!ValidateEmailAddress(formField.Field)) {
      errorSet.push(formField);
      allValid = false;
   }
}
if (!allValid) {
   DisplayErrorSet(errorSet);
   return false;
}
return true;
}
function submitForm(elqForm) {
   if (CheckElqForm(elqForm)) {
       prepareSelectsForEloqua(elqForm);
       fnPrepareCheckboxMatricesForEloqua(elqForm);
       return true;
   }
   else { return false; }
}
function prepareSelectsForEloqua(elqForm) {
   var selects = elqForm.getElementsByTagName("SELECT");
   for (var i = 0; i < selects.length; i++) {
       if (selects[i].multiple) {
           createEloquaSelectField(elqForm, selects[i]);
       }
   }
   return true;
}
function createEloquaSelectField(elqForm, sel) {
   var inputName = sel.name;
   var newInput = document.createElement('INPUT');
   newInput.style.display = "none";
   newInput.name = inputName;
   newInput.value = "";
   for (var i = 0; i < sel.options.length; i++) {
       if (sel.options[i].selected) {
           newInput.value += sel.options[i].value + "::";
       }
   }
   if (newInput.value.length > 0) {
       newInput.value = newInput.value.substr(0, newInput.value.length - 2);
   }
   sel.disabled = true;
   newInput.id = inputName;
   elqForm.insertBefore(newInput, elqForm.firstChild);
}
function appendContentID() {
   var frm=document.forms['GV11FoDFoDMicrosite8427'];
   if (frm) {
      var re = new RegExp("[?&](gatedcontent)=([^&$]*)", "i" );
      var offset = location.search.search( re );
      if (offset == -1)
         return null;
      else {
         var contentID=document.createElement('input');
         contentID.setAttribute('type', 'hidden');
         contentID.setAttribute('name', 'elqGatedContent');
         contentID.setAttribute('value', RegExp.$2);
         frm.appendChild(contentID);
      }
   }
}
function fnPrepareCheckboxMatricesForEloqua(elqForm) {
   var matrices = elqForm.getElementsByTagName('TABLE');
   for (var i = 0; i < matrices.length; i++) {
       var tableClassName = matrices[i].className;
       if (tableClassName.match(/elqMatrix/)) {
           if (fnDetermineMatrixType(matrices[i]).toLowerCase() == 'checkbox') {
               if (matrices[i].rows[0].cells[0].childNodes.length == 1) {
                   if (matrices[i].rows[0].cells[0].childNodes[0].nodeName != '#text') {
                       fnCreateHorizontalMatrixCheckboxField(elqForm, matrices[i]);
                   }
                   else {
                       fnCreateVerticalMatrixCheckboxField(elqForm, matrices[i]);
                   }
               }
           }
       }
   }
   return true;
}
function fnCreateVerticalMatrixCheckboxField(elqForm, matrix) {
   var inputName = matrix.id + 'r' + 1;
   var newInput = document.createElement('INPUT');
   newInput.style.display = 'none';
   newInput.name = inputName;
   newInput.value = '';
   var inputs = document.getElementsByName(inputName);
   for (var i=0; i < inputs.length; i++) {
       if (inputs[i].nodeName.toLowerCase() == 'input') {
           if (inputs[i].checked == true) {
               if (inputs[i].type.toLowerCase() == 'checkbox') {
                   newInput.value += inputs[i].value + '::';
                   inputs[i].disabled = true;
               }
           }
       }
   }
   if (newInput.value.length > 0) {
       newInput.value = newInput.value.substr(0, newInput.value.length - 2);
   }
   newInput.id = inputName;
   elqForm.insertBefore(newInput, elqForm.firstChild);
   matrix.disabled = true;
}
function fnCreateHorizontalMatrixCheckboxField(elqForm, matrix) {
   for (var i=1; i < matrix.rows.length; i++) {
       var inputs = document.getElementsByName(matrix.id + 'r' + i);
       var oMatrixRow = matrix.rows[i];
       var inputName = oMatrixRow.id;
       var newInput = document.createElement('INPUT');
       newInput.style.display = 'none';
       newInput.name = inputName;
       newInput.value = '';
       for (var j=0; j < inputs.length; j++) {
           if (inputs[j].nodeName.toLowerCase() == 'input') {
               if (inputs[j].checked == true) {
                   if (inputs[i].type.toLowerCase() == 'checkbox') {
                       newInput.value += inputs[j].value + '::';
                       inputs[j].disabled = true;
                   }
               }
           }
       }
       if (newInput.value.length > 0) {
           newInput.value = newInput.value.substr(0, newInput.value.length - 2);
       }
       newInput.id = inputName;
       elqForm.insertBefore(newInput, elqForm.firstChild);
   }
   matrix.disabled = true;
}
function fnDetermineMatrixType(oTable) {
   var oFirstMatrixInput = oTable.rows[1].cells[1].childNodes[0];
   return oFirstMatrixInput.type;
}

</SCRIPT>
          <FORM id="GV11FoDFoDMicrosite8427" onSubmit="return submitForm(this);" method="post" name="GV11FoDFoDMicrosite8427" action="http://now.eloqua.com/e/f2.aspx">
		  <input type="hidden" name="source" value="<?php echo $_GET['source']; ?>">
            <INPUT value="GV11FoDFoDMicrosite8427" type=hidden name="elqFormName">
            <INPUT value="903" type="hidden" name="elqSiteID">
            <SCRIPT language="JavaScript">
document.write("<input type=\"hidden\" id=\"PageReferrer\" name=\"PageReferrer\" value=\"" + ((p_source == "") ? "Unknown" : p_source) + "\">")
              </SCRIPT>
            </P>
            <TABLE border=0 width=450>
              <TBODY>
                <TR>
                  <TD width=160><SPAN class=form_text>First Name:</SPAN></TD>
                  <TD><INPUT style="WIDTH: 230px" id="FirstName" class="form_text" size=40 value="" name="FirstName">
                    <SPAN style="COLOR: #ed1c24; FONT-SIZE: 9px">*</SPAN></TD>
                </TR>
                <tr>
                  <td><span class="form_text">Last Name:</span></td>
                  <td><INPUT class="form_text" id="LastName" style="WIDTH: 230px" size="40" value="" name="LastName">
                    <span style="font-size: 9px; color: #ed1c24">*</span></td>
                </tr>
                <tr>
                  <td><span class="form_text">Agency/Organization:</span></td>
                  <td><INPUT class="form_text" id="Company" style="WIDTH: 230px" size="40" value="" name="Company">
                    <span style="font-size: 9px; color: #ed1c24">*</span></td>
                </tr>
                <tr>
                  <td><span class="form_text">Industry:</span></td>
                  <td><SELECT class="form_text" id="Industry" name="Industry" style="WIDTH: 200px" onChange="expandDiv()">
                      <option value="" selected>-- Please Select --</option>
                      <OPTION value="Government - Federal">Government - Federal</OPTION>
					  <OPTION value="Government – State/Local">Government – State/Local</OPTION>
					  <OPTION value="Other">Other</OPTION>
                    </SELECT>
                    <span style="font-size: 9px; color: #ed1c24"> *</span></td>
                </tr>
                <tr>
                  <td><span class="form_text">Phone:</span></td>
                  <td><input class="form_text" id="Phone" style="WIDTH: 230px"  size=40 value="" name="Phone" />                    <span style="font-size: 9px; color: #ed1c24">*</span></td>
                </tr>
                <tr>
                  <td><span class="form_text">Email Address:</span></td>
                  <td><INPUT class="form_text" id="EmailAddress" style="WIDTH: 230px"  size="40" value="" name="EmailAddress">
                    <span style="font-size: 9px; color: #ed1c24">*</span></td>
                </tr>
                <TR>
                  <TD colSpan=2 align=center><br /><INPUT id="submit" class="elqSubmit" title="Submit" value="Submit" alt="Submit" src="http://img.en25.com/eloquaimages/clients/lexisnexis/%7b967d664e-ee2e-4a7d-9c47-f69d62a16b77%7d_img_submit.gif" type="image" name="submit"></TD>
                </TR>
              </TBODY>
            </TABLE>
          </FORM>
    
<script type='text/javascript' language='JavaScript' src='http://lexisnexis.com/risk/elqNow/elqCfg.js'></script>
<script type='text/javascript' language='JavaScript' src='http://lexisnexis.com/risk/elqNow/elqImg.js'></script>

