<?php
/**
 * Homepage template (formerly region-specific homepage).
 *
 * Template Name: Home Page
 */
get_header();?>
<script src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.matchHeight.js"></script>
<!-- <script src="xxxxxx/js/jquery.cycle.all.js"></script> -->
<script src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.cycle2.js"></script>
<script src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.cycle2.carousel.js"></script>
<script src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.easing.js"></script>
<script>
var firstTimeHomeSliderRuns = true;

(function($) {
  $(function() {

    $('.interactive-slide.ln-slide').on('click',function(evt){
      var url =$(evt.currentTarget).data("url");
      if(url==undefined)return;
     window.location.href = url;
    });

    $('.slideshow').show().cycle({
        // fx:                 "carousel"
        fx:                "fade"
        ,carouselVisible:   true
        ,carouselFluid:     true
        ,pager:             "#slider-pager"

        //,easing:             'easeInOutExpo'
        ,speed:             1000
        ,timeout:           8000
        ,pagerTemplate:     "<a class='pager-cell' href=#></a>" // "<strong><a href=#> {{slideNum}} </a></strong>"
    });

    $('#sponsorships .rotator').show().cycle({
      fx:'fade',
      timeout:5000
    });

    $('.home-item').matchHeight();
  });
}(jQuery));
</script>




<?php
  query_posts(array('post_type' => 'slide', 'posts_per_page' => -1));
  if (have_rows('slides')) {
?>




<!-- slider-wrapper -->
<!-- <div id="slider-wrapper" ></div> -->

<!-- LN-RESPONSIVE-SLIDER -->
<div class="row" style="margin-bottom:20px;">
  <div class="col-12">


<div class="slideshow-responsive-box">
    <div class="slideshow responsive" >
        <!-- images -->
<?php
    $clickClass = "interactive-slide ln-slide";
    while (have_rows('slides')) {
      the_row();
      $caption      = get_sub_field('caption');
      $url          = get_sub_field('url');
      $image        = get_sub_field('image')['url'];
      $sliderClass  = (!empty($url))? $clickClass : "ln-slide";
      //
      echo "<img class=\"".$sliderClass."\" src=\"".$image."\" alt=\"".$caption."\" data-url=\"".$url."\"/>";
    }
   // wp_reset_query();
?>
    </div><!-- .slideshow -->
    <div id="slider-pager" class="external cycle-pager"></div>
</div><!-- slideshow-responsive-box -->
</div> <!-- col-12 -->
</div> <!-- row -->


<?php
} else {
   // echo '<div style="height: 30px;background: #fff;"></div>';
}
wp_reset_query();


?>


<div class="home-stripe">
	<div class="container">
		<div class="row">
			<div class="col-md-9">
				<h2>Recent Blog Posts</h2>
			</div>
			<div class="col-md-3">
				<a href="/healthcare/archive/" title="View Archive" class="archive-link ">View Archive</a>
			</div>
		</div>
	</div>
</div>


<div class="container">
<div class="row">
<div id="primary" class="col-md-9">

 <div id="content" role="main">
 <div class="article-grid">

      <?php
      $post_count = intval(get_field('post_count'));
      $recent = new WP_Query(array(
        'order'   => 'desc',
        'orderby' => 'date',
        'posts_per_page' => ($post_count > 0) ? $post_count : 5
      ));

      $odd = 0;
    	while ($recent->have_posts()):
        $recent->the_post();
        $id = get_the_ID();
        // These no longer work correctly within WP_Query, so we use
        // get_the_terms instead:
        // $categories = wp_get_post_categories($id);
        // $categories = wp_get_object_terms($id, 'category');
        $categories = get_the_terms($id, 'category');
          $categ = '';
        if (count($categories) > 0) {
          /*
          $categ = implode(', ', array_map(function($category) {
            return $category->name;
          }, $categories));

          */
          $categ = $categories[0]->name;
        } 
    		$url = get_permalink($id);
  		?>
      <div class="tile">
        <div class="inner">
			<div class="image-wrapper"><?php the_post_thumbnail( array(161,100) ); ?></div>
          <div class="caption">
            <div class="date"> <?php echo ucfirst(get_the_date('F j, Y','','',FALSE)); ?> | <?php echo $categ; ?></div>
            <div class="title"><a href="<?php if (empty($url)) { the_permalink(); } else { echo $url; } ?>"><?php the_title(); ?></a></div>
            <div class="expert"><?php the_expert(Array('show_link' => false, 'id' => $id)); ?></div>
          </div>
        </div>
      </div>
  		<?php
    	endwhile;
      wp_reset_postdata();
      ?>
  </div><!-- .grid -->

    <div class="bottom-more">
      <a href="/healthcare/archive/">View Archive</a>
    </div>
  </div><!-- #content -->
</div><!-- #primary -->

<?php get_sidebar(); ?>

</div><!-- row -->
</div><!-- container -->

<?php get_footer(); ?>
