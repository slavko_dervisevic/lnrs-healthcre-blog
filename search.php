<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package WordPress
 * @subpackage LexisNexis_WP_Theme
 * @since LexisNexis WP Theme 1.0
 */
//echo phpinfo();
get_header(); ?>
<div class="row">
<section id="primary" class="col-sm-8">
    <!-- <section id="primary"> -->
      <div id="content" role="main">

      <?php if ( have_posts() ) : ?>

        <header class="page-header">
          <h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'lnwptheme' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
        </header>

        <?php //lnwptheme_content_nav( 'nav-above' ); ?>

        <?php /* Start the Loop */ ?>
        <?php while ( have_posts() ) : the_post(); ?>

          <?php
            get_template_part( 'content', 'excerpt' );
          ?>

        <?php endwhile; ?>

        <?php lnwptheme_content_nav( 'nav-below' ); ?>

      <?php else : ?>

        <article id="post-0" class="post no-results not-found">
          <header class="entry-header">
            <h1 class="entry-title"><?php _e( 'Nothing Found', 'lnwptheme' ); ?></h1>
          </header><!-- .entry-header -->

          <div class="entry-content">
            <p><?php _e( 'Sorry, but nothing matched your search criteria. Please try again with some different keywords.', 'lnwptheme' ); ?></p>
            <?php get_search_form(); ?>
          </div><!-- .entry-content -->
        </article><!-- #post-0 -->

      <?php endif; ?>

      </div><!-- #content -->
    </section><!-- #primary -->
  </div> <!-- row -->

<?php // get_sidebar(); // Disable sidebar on search page ?>
<?php get_footer(); ?>
