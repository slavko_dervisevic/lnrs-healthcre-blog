<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage LexisNexis_WP_Theme
 * @since LexisNexis WP Theme 1.0
 */

get_header(); ?>


<div class="header-stripe">
  <div class="container">
    <header class="entry-header">
      <h1 class="entry-title"><?php the_title(); ?></h1>
    </header>
  </div>
</div>

<div class="breadcrumb-stripe">
  <div class="container">
  	<?php if(function_exists('simple_breadcrumb')) {simple_breadcrumb();} ?>
  </div>
</div>

  <div class="container">
  <div class="row">
    <div id="primary" class="col-md-8">
      <div id="content" role="main">

        <?php the_post(); ?>

        <?php get_template_part( 'content', 'page' ); ?>

        <?php //comments_template( '', true ); ?>

      </div><!-- #content -->
    </div><!-- #primary -->

    <?php get_sidebar(); ?>
  </div> <!-- .row -->
  </div> <!-- .container -->
<?php get_footer(); ?>
