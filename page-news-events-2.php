<?php
/**
 * Template Name: News & Events 2
 *
 * @package WordPress
 * @subpackage LexisNexis_WP_Theme
 */

get_header();
?>

<div id="primary">
    <div id="content" role="main">

        <article id="post-<?php the_post(); the_ID(); ?>" <?php post_class(); ?>>
            <header class="entry-header">
                <h1 class="entry-title"><?php the_title(); ?></h1>
            </header>
            <!-- .entry-header -->

            <div class="entry-content">

<?php
                            query_posts(array('post_type' => 'event', 'posts_per_page' => -1, 'order'    => 'ASC'));
    $first = true;
    if (have_posts()) {
        echo '<ul style="margin-left:0;">';
        while (have_posts()) {
            the_post();
            ?>
            <li <?php //if (has_post_thumbnail(get_the_ID())) echo ' class="first-with-thumb"'; ?>>
                <?php if (true /*$first*/) {
                $first = false;
                //the_post_thumbnail();
            } ?>
                <a target="_blank"
                   href="<?php echo get_post_meta(get_the_ID(), 'url', true); ?>"><?php the_title(); ?></a>
                <br>

                <div class="date"><?php echo get_post_meta(get_the_ID(), 'date', true); ?></div>
                <div class="location"><?php echo get_post_meta(get_the_ID(), 'location', true); ?></div>
				<div class="attend"><?php echo get_post_meta(get_the_ID(), 'attend', true); ?></div>
            </li>
            <?php

        }
        echo '</ul>';
    } wp_reset_query(); ?>


            </div>
        </article>

    </div>
    <!-- #content -->
</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>