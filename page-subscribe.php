<?php
/**
 * Template Name: Subscribe Form
 */

get_header(); ?>

<div class="breadcrumb-stripe">
  <div class="container">
  	<?php if(function_exists('simple_breadcrumb')) {simple_breadcrumb();} ?>
  </div>
</div>

<div class="subscribe-stripe">
<div class="container">
    <div id="primary">
      <div id="content" role="main">

        <?php the_post(); ?>

        <?php get_template_part( 'content', 'page' ); ?>

        <div class="contact-form">
          <?php include_once 'contactform/subscribe-form.html'; ?>
        </div>

        <?php //comments_template( '', true ); ?>

      </div><!-- #content -->
    </div><!-- #primary -->

</div> <!-- container -->
</div> <!-- stripe -->
<?php get_footer(); ?>
