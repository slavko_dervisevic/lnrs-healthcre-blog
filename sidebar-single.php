<?php
/**
 * The Sidebar containing the main widget area.
 *
 * @package WordPress
 * @subpackage LexisNexis_WP_Theme
 * @since LexisNexis WP Theme 1.0
 */

function renderSpotlightPost($post) {
?>
<div style="clear: both;">
  <p><a href="<?php echo get_permalink($post); ?>"><?php echo $post->post_title . get_the_expert(Array('show_title' => false, 'show_link' => false, 'id' => $post->ID, 'prefix' => ", By ")); ?></a><br /></p>
</div>
<?php
}

function renderPost($post) {
?>
<div style="clear: both;">
  <p><a href="<?php echo get_permalink($post); ?>"><?php echo $post->post_title . get_the_expert(Array('show_title' => false, 'show_link' => false, 'id' => $post->ID, 'prefix' => ", By ")); ?></a><br /></p>
</div>
<?php
}

?>
<div id="secondary" class="widget-area" role="complementary">
<?php
  if (is_page()) {
    // Legacy 'Our Experts' code (may not even be in PS, only FOTD?)
    /*
    global $post;

    $children = get_pages('child_of=' . $post->ID);

    if (count($children) > 0) {
      echo '<aside class="widget"><div id="pages-menu">';
      if (is_page('about') || $post->post_parent == 27) {
        echo '<h3 class="widget-title">Our Experts</h3>';
      }
      echo '<ul>';
      wp_list_pages(array('child_of' => $post->ID, 'title_li' => '', 'link_after'=>'<span class="new"></span>'));
      echo '</ul>';
      echo '</div></aside>';
    }
    else if ($post->post_parent) {
      //$parent = get_post($post->post_parent);
      echo '<aside class="widget"><div id="pages-menu">';
?>
      <script>
      jQuery(document).ready(function() {
        jQuery('#access a[href="<?php echo get_permalink($post->post_parent); ?>"]').parent().addClass('current-menu-item');
      });
      </script>
<?php
      if (is_page('about') || $post->post_parent == 27) {
        echo '<h3 class="widget-title">Our Experts</h3>';
      }
      echo '<ul>';
      wp_list_pages(array('child_of' => $post->post_parent, 'title_li' => '', 'link_after'=>'<span class="new"></span>'));

      echo '</ul>';
      echo '</div></aside>';
    }
    */

    // Render the "All Pages" sidebar:
    dynamic_sidebar('sidebar-pages');

    //

    if (is_page('home3')) {
      global $wp_query;
      // Render Spotlight:
      if (have_rows('spotlight', $wp_query->post->ID)):
?>
<aside class="widget widget_spotlight">
  <h3 class="widget-title">Spotlight</h3>
<?php
while (have_rows('spotlight', $wp_query->post->ID)): the_row();
  $post = get_sub_field('post');
  renderSpotlightPost($post);
endwhile;
?>
</aside>
<?php
      endif;

      // Render the rest of the homepage-specific sidebar:
      dynamic_sidebar('homepage-sidebar');
    } // is_front_page()
  } // is_page()
  else if (is_single()) {
    dynamic_sidebar('sidebar-pages');
    dynamic_sidebar('sidebar-single');

    // Dynamic "Related Articles" section:
    // TODO: Should probably be built as a widget?
?>
    <aside class="widget widget_spotlight">
      <h3 class="widget-title">Related Articles</h3>
<?php
    global $wp_query;
    wp_reset_query();
    if (have_rows('related_articles', $wp_query->post->ID)) {
      while (have_rows('related_articles', $wp_query->post->ID)): the_row();
        $post = get_sub_field('post');
        renderPost($post);
      endwhile;
    }
    else {
      // No related articles are specified; find the three latest articles (that
      // aren't this article) within the category and render those instead.
      $categories = get_the_category();
      $recent_posts = get_posts(array(
        'category' => $categories[0]->cat_ID,
        'posts_per_page' => 3,
        'exclude' => $wp_query->post->ID
      ));
      foreach ($recent_posts as $idx => $post) {
        renderPost($post);
      }
      wp_reset_query();
    }
?>
    </aside>
<?php
  } // is_single()
  else {
    dynamic_sidebar('sidebar-pages');
  } ?>
</div><!-- #secondary .widget-area -->
