<?php
/**
 * Template Name: Our Experts
 */
get_header(); ?>

<div class="header-stripe">
  <div class="container">
    <header class="entry-header">
      <h1 class="entry-title"><?php the_title(); ?></h1>
    </header>
  </div>
</div>

<div class="breadcrumb-stripe">
  <div class="container">
  	<?php if(function_exists('simple_breadcrumb')) {simple_breadcrumb();} ?>
  </div>
</div>


<div class="container">
<div class="row">
<div id="primary" class="col-md-9">

  <div id="content" role="main">
    <div class="content"><?php the_content(); ?></div>
    <?php while (have_rows('experts', 'option')):
      the_row();
      if (!get_sub_field('visible')) continue;
    ?>
    <a id="<?php echo urlencode(get_sub_field('name')); ?>"></a>
    <div class="user">
      <?php $image = get_sub_field('photo'); if ($image): ?>
        <img src="<?php echo esc_url($image['url']); ?>" alt="<?php esc_attr(the_sub_field('name')) ?>" class="alignleft size-medium" />
      <?php endif; ?>
      <div class="inner">
      <h2><?php the_sub_field('name'); ?></h2>
      <p><strong><?php the_sub_field('job_title'); ?></strong></p>
      <?php echo wpautop(get_sub_field('biography')); ?>
      </div>
    </div>
    <?php endwhile ?>
  </div><!-- #content -->
</div><!-- #primary -->

<?php get_sidebar(); ?>
</div><!-- row -->
</div><!-- container -->
<?php get_footer(); ?>
