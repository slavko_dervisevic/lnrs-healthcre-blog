<?php
/**
 * The Sidebar containing the main widget area.
 *
 * @package WordPress
 * @subpackage LexisNexis_WP_Theme
 * @since LexisNexis WP Theme 1.0
 */
?>
<div id="secondary" class="widget-area" role="complementary">

    <?php dynamic_sidebar('homepage-sidebar'); ?>
    <?php 
    <aside class="widget">
        <h3 class="widget-title">LexisNexis Insurance Exchange</h3>
        <div class="content">
            <p>Eliminate inefficient data entry and improve  process flow.</p>
            <a href="#" class="red-link-button">Read More</a>
        </div>
    </aside>

    <aside id="connect-with-us" class="widget">
        <h3 class="widget-title">Connect With Us</h3>

        <div class="content">
            <p>Sign up to get updates through social media channels.</p>

            <div class="links">
                <a class="twitter" href="#"></a>
                <a class="rss" href="<?php bloginfo('rss_url'); ?>"></a>
                <a class="linkedin" href="#"></a>
                <a class="facebook" href="#"></a>

                <div class="clear"></div>
            </div>

            <h4>Subscribe</h4>

            <p>Join our mailing list for the latest industry news, expert opinions, industry events and special offers
                from LexisNexis®.</p>

            <form action="" id="subscribe">
                <input type="text" name="email" id="subscribe-email" class="text">
                <a onclick="jQuery('form#subscribe').submit();" class="red-link-button">Subscribe</a>

                <div class="clr"></div>
            </form>
        </div>
    </aside> */?>

</div><!-- #secondary .widget-area -->
