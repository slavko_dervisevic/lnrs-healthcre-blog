<?php
$cat = get_term_by('name', single_cat_title('',false), 'category');
$catSlug = $cat->slug;
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('post-excerpt'); ?>>

    <div class="entry-meta thumb">
<?php if (has_post_thumbnail()): ?>
      <?php the_post_thumbnail('post-thumbnail-smaller'); ?>
<?php else: ?>
    	<img src="<?php echo bloginfo('stylesheet_directory') . '/images/' . $catSlug; ?>-post.jpg" />
<?php endif; ?>
    </div>

    <header class="entry-header">
        <h2 class="entry-title">
            <a href="<?php the_permalink(); ?>" title="<?php printf(esc_attr__('Permalink to %s', 'lnwptheme'), the_title_attribute('echo=0')); ?>" rel="bookmark"><?php the_title(); ?></a>
        </h2>
        <p><?php the_expert(); ?></p>
        <!-- .entry-meta -->
    </header>
    <!-- .entry-header -->


    <div class="entry-summary" style="padding-top: 0;">
		<?php global $post;
			if ($post->post_type == 'post') {
		} ?>

        <?php echo substr(strip_tags(get_the_content('Read more')), 0, 250) . '...'; ?>
        <div class="read-more"><a style="font-size:12px;" class="red-link-button" href="<?php the_permalink(); ?>">Read More</a></div>
        <?php wp_link_pages(array('before'=>'<div class="page-link"><span>' . __('Pages:', 'lnwptheme') . '</span>', 'after'=>'</div>')); ?>
    </div>
    <!-- .entry-content -->


    <footer class="entry-meta">
        <?php $categories_list = get_the_category_list(__(', ', 'lnwptheme'));
        $tag_list = get_the_tag_list('', __(', ', 'lnwptheme')); ?>

        <?php if (!empty($categories_list)) { ?>
            <div class="categories" style="display:none;">
              Posted in <?php echo $categories_list; ?>
            </div>
        <?php } ?>

        <?php if (!empty($tag_list)) { ?>
            <div class="tags">
                Tags: <?php echo $tag_list; ?>
            </div>
        <?php } ?>

        <?php edit_post_link(__('Edit', 'lnwptheme'), '<span class="edit-link">', '</span>'); ?>
    </footer>

    <div class="clear"></div>
</article><!-- #post-<?php the_ID(); ?> -->
