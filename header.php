<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head <?php do_action( 'add_head_attributes' ); ?>> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage LexisNexis_WP_Theme
 * @since LexisNexis WP Theme 1.0
 */
?><!DOCTYPE html>
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head <?php do_action( 'add_head_attributes' ); ?>>
    <meta http-equiv=”X-UA-Compatible” content=”IE=edge” />
    <meta charset="<?php bloginfo('charset'); ?>"/>
    <meta name="viewport" content="width=device-width"/>
    <title><?php
    /*
   * Print the <title> tag based on what is being viewed.
   */
        global $page, $paged;

        wp_title('-', true, 'right');

        // Add the blog name.
        bloginfo('description');

        // Add a page number if necessary:
        if ($paged >= 2 || $page >= 2){
            echo ' | ' . sprintf(__('Page %s', 'ln-ps-wp-theme'), max($paged, $page));
    }
        ?></title>
        <link REL="SHORTCUT ICON" HREF="https://risk.lexisnexis.com/Areas/LNRS/favicon.ico">
    <link rel="profile" href="https://gmpg.org/xfn/11"/>
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">
    <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_url'); ?>"/>
    <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_directory'); ?>/style-ln.css"/>
    <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_directory'); ?>/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/js/css/ui-lightness/jquery-ui.css">
    <!--link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_directory'); ?>/css/bsors.css"/-->
    <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_directory'); ?>/css/bsnew.css"/>
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>"/>
    <!--[if lt IE 9]>
    <script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
    <![endif]-->
<?php
    if (is_singular() && get_option('thread_comments'))
    wp_enqueue_script('comment-reply');
    wp_enqueue_script('jquery');
    wp_enqueue_script('jquery-ui');

    wp_head();
    ?>

    <!-- jquery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.0/jquery-ui.min.js"></script>

    <!-- bs4 includes -->
    <script type='text/javascript' src="<?php bloginfo('stylesheet_directory'); ?>/js/bootstrap.bundle.min.js"/></script>
    <!--script type='text/javascript' src="<?php bloginfo('stylesheet_directory'); ?>/js/popper.min.js" /></script-->
    <script type='text/javascript' src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.ellipsis.min.js" /></script>

    <script>
        jQuery(document).ready(function() {
            jQuery('#branding #s').val('Search...');
            jQuery('#branding #s').focus(function() {
                if (this.value == 'Search...')this.value = '';
            });
            jQuery('#branding #s').blur(function() {
                if (this.value == '')this.value = 'Search...';
            });
			
        <?php if (is_single()) { ?>
            jQuery('#access li a:contains("Blog")').parent().addClass('current-menu-item');
            <?php } ?>
            /*
            jQuery('#ie7 #access div > ul > li:last a').css('border-right', 'none');
            jQuery('#ie7 #secondary .widget:last').css({ 'margin-bottom' : 0, 'padding-bottom' : 0, 'border-bottom' : 'none'});
            jQuery('#ie7 #secondary .ui-tabs .snippet:last').css({ 'margin-bottom' : 0});
            jQuery('#ie7 #home-latest-blog-posts .snippet:last').css({ 'margin-bottom' : 0});
            jQuery('#ie7 .page-template-page-blog-php #content .hentry:last').css({ 'margin-bottom' : 0, 'padding-bottom' : 0, 'border-bottom' : 'none'});
            jQuery('#ie7 #brokers .content .customer:last').css({ 'margin-bottom' : 0, 'padding-bottom' : 0, 'border-bottom' : 'none'});
            jQuery('#ie7 .blogger:last').css({ 'margin-bottom' : 0, 'padding-bottom' : 0, 'border-bottom' : 'none'});
            jQuery('#ie7 #carriers .content .customer:last').css({ 'margin-bottom' : 0, 'padding-bottom' : 0, 'border-bottom' : 'none'});
            jQuery('#ie7 #main #content .customer div.links').each(function() {
                jQuery(this).find('a:last').css({ 'padding-right' : 0, 'border-right' : 'none'})
            });
            jQuery('#ie7 .hentry:last').css({ 'border-bottom' : 'none'});
            jQuery('#ie7 #main #secondary ul li:last').css('margin-bottom', 0);
      jQuery('#ie7 #brokers-carriers .links a:first-child').css({'border-right' : '1px solid #e5e5e5', 'padding-left': '0'});

      jQuery('#ie7 .posts-table thead th:last').css('border-right', '1px solid #cfcccc');
            */
      jQuery('#ie8 .posts-table thead th:last').css('border-right', '1px solid #cfcccc');

            jQuery('#content .hentry .entry-content > h2:first-child').css({'margin-top' : 0});
      <?php if (is_home() || is_front_page()) { ?>
      var height1 = jQuery('#home-top-row #home-latest-blog-posts > .content').height();
      var height2 = jQuery('#home-top-row #home-latest-news > .content').height();
      var max = height1;
      if (height2 > height1) max = height2;
      jQuery('#home-top-row #home-latest-blog-posts > .content').css('height', max + 'px');
      jQuery('#home-top-row #home-latest-news > .content').css('height', max + 'px');
      <?php } ?>
        });
    </script>
    <?php echo htmlspecialchars_decode(stripslashes_deep(get_option('lexisnexisblog-scripts-header'))); ?>


</head>

<body <?php body_class(); ?>>

<div id="page" class="hfeed">

<header id="branding" role="banner">
<div class="container">
  <div class="navbar navbar-expand-lg ">


<!-- LOGO -->
    <a id="logo" href="<?php echo home_url(); ?>"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/logo_lexis.png" alt="LexisNexis Logo"></a>

<!-- BLOG-NAME -->
    <div id="blog-name"><a href="<?php echo home_url(); ?>"><?php bloginfo('name'); ?></a></div>

<!-- HAMBURGER BUTTON -->
    <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
    </button>

<!-- CONTENT-COLLAPSE -->
    <div class="collapse navbar-collapse" id="navbarCollapse">
		<div class="navbar-inner">
			<div class="top-nav">
<!-- SEARCH -->
				<div class="top-search">
					<div class="opener"></div>
					<?php get_search_form(); ?>
				</div>
<!-- END SEARCH FORM -->
				<a href="https://risk.lexisnexis.com/" title="" class="corporate-link">Visit Corporate Site</a>
			</div>
 <!-- MENU -->
			<nav id="access" role="navigation">
				<?php wp_nav_menu(); ?>
			</nav>
		</div>

  </div> <!-- end navbar-collapse -->

</div> <!-- end navbar-expand-md -->
</div> <!-- end container -->

</header>



    <div id="main">
