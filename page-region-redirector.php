<?php
/**
 * The region redirector performs a 302 (temporary) redirect to your region's
 * homepage.
 *
 * Template Name: Region Redirector
 */
header('Location: ' . get_permalink(get_region_homepage()->ID), true, 302);
exit;
