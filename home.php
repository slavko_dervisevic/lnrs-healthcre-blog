<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * Template Name: Home
 * @package WordPress
 * @subpackage LexisNexis_WP_Theme
 */

get_header();?>
<script src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.matchHeight.js"></script>
<script src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.cycle.all.js"></script>
<script src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.easing.1.3.js"></script>
<script>
var firstTimeHomeSliderRuns = true;


(function($) {
  $(function() {
    $('#slider').cycle({
        //prev: '#prev',
        //next: '#next',
        containerResize: 0,
        width: '100%',
        fit: 0,
slideResize: 0,
        fx:'fade',
        /*easing: 'easeInOutExpo',*/
        speed:1000,
        /*before: slideRebBlockLeft,*/
        timeout:8000,
        pager:'#slider-pager'
    });

    $('#sponsorships .rotator').show().cycle({
      fx:'fade',
      timeout:5000
    });

    $('.home-item').matchHeight();
  });
}(jQuery));
</script>
<?php
query_posts(array('post_type' => 'slide', 'posts_per_page' => -1));
if (have_posts()) {
    ?>
<div id="slider-wrapper">
    <div id="slider">
        <?php
        while (have_posts()) {
            the_post();
            ?>
            <div class="slide pumper-slide-<?php echo get_the_ID(); ?>">
                
              
                <?php the_post_thumbnail('post-thumbnail', ['class' => 'img-fluid']);?>
                
                <div class="insight_bar">
                    <h3><?php the_title();?></h3>
        
                    <div class="text">
                        <?php echo get_post_meta(get_the_ID(), 'banner_text', true);?>
                    </div>
                    
                    <?php 
                    $url = get_post_meta(get_the_ID(), 'banner_url', true);
                    $target = get_post_meta(get_the_ID(), 'target', true);
                    $target = ($target ? ' target="' . $target . '"' : '');
                    if ($url) {
                        ?><div class="clear"></div>
                         <a href="<?php echo $url; ?>" <?php echo $target; ?>><span class="red-link-button">Learn More</span></a><?php
                    } ?>
                </div>
            </div>
            <?php } ?>
    </div>
        <div id="slider-pager"></div>
        <?php
    
    } else {
        echo '<div style="height: 30px;background: #fff;"></div>';
    }
    wp_reset_query();
    ?>
</div>


<div id="primary">
  <div id="content" role="main">
    <div class="clear"></div>        
<?php 
$i = 0;
while (have_rows('featured_posts')): the_row();
  $item = get_sub_field('post');
  $user = get_userdata($item->post_author);
  $categories = get_the_category($item->ID);
?>
    <div class="home-item">
      <div class="title">
        <a href="<?php echo get_permalink($item); ?>"><?php echo $categories[0]->name; ?></a>
      </div>
      <div class="thumb">
        <a href="<?php echo get_permalink($item); ?>"><?php if (has_post_thumbnail($item)) echo get_the_post_thumbnail($item, 'post-thumbnail-small'); ?></a>
      </div>
      <div class="excerpt">
        <strong><a href="<?php echo get_permalink($item); ?>"><?php echo $item->post_title; ?></a></strong>
      </div>
      <div class="auth">
        By <?php echo $user->first_name . ' ' . $user->last_name; ?>
      </div>
    </div>
<?php
  if ($i % 2) echo '<div class="clear"></div>' . "\n";
  $i++;
endwhile;
?>
    <div class="clear"></div>
  </div><!-- #content -->
</div><!-- #primary -->


<?php get_sidebar(); ?>
<?php get_footer(); ?>