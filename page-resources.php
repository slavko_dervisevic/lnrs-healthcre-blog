<?php
/**
 * Template Name: Resources
 *
 * @package WordPress
 * @subpackage LexisNexis_WP_Theme
 */
get_header();

function getResources($termId){
	global $wpdb, $wp_locale;

	// Wut.  Thanks, Emarri, for this mess.  Ugh.
	$query_string = "SELECT SQL_CALC_FOUND_ROWS $wpdb->posts.ID, $wpdb->posts.post_title, $wpdb->posts.post_content
	FROM $wpdb->posts
	LEFT JOIN $wpdb->term_relationships ON ($wpdb->posts.ID = $wpdb->term_relationships.object_id)
	LEFT JOIN $wpdb->term_taxonomy ON ($wpdb->term_relationships.term_taxonomy_id = $wpdb->term_taxonomy.term_taxonomy_id)
	LEFT JOIN $wpdb->terms ON($wpdb->term_taxonomy.term_id = $wpdb->terms.term_id)
	WHERE 1=1
	AND $wpdb->posts.post_type = 'resource'
	AND ($wpdb->posts.post_status = 'publish' OR $wpdb->posts.post_status = 'private')
	AND $wpdb->term_taxonomy.taxonomy = 'resource_type'
	AND $wpdb->term_taxonomy.term_id = $termId
        AND $wpdb->posts.post_date > 04-01-2016
	ORDER BY $wpdb->posts.post_date DESC";
	return $wpdb->get_results($query_string, OBJECT_K);
}


// Extracted render code.  A copypasta mess of the gibberish before I got here, with some improvements.
// Why did he think it was a good or expeditious idea to _hardcode_ term IDs?  What Wordpress developer doesn't use ACF nowadays?
function renderResource($post) {
	$id = $post->ID;
	$title = $post->post_title;
	$content = $post->post_content;
	$url = array_shift(get_post_meta($id, 'url'));
	$thumb = get_the_post_thumbnail( $id, 'thumbnail' );
?>
<li style="background: none; margin-bottom: 10px; padding-left: 0;">
<?php if (!is_null($thumb)){ ?>
  <div class="thumb fl" style="margin:0 15px 10px 0;">
    <?php echo $thumb; ?>
  </div>
<?php } ?>
  <div class="content fl" style="width: 85%;">
<?php if (get_field('purchasable', $id)) { ?>
    <strong><a href="#preview-content-<?php echo $id; ?>" class="fancybox-inline"><?php echo $title; ?></a></strong>
<?php } else { ?>
    <strong><a href="<?php echo $url; ?>" target="_blank"><?php echo $title; ?></a></strong>
<?php } ?>
    <br />
    <?php echo $content; ?>
  </div>
  <div class="clear"></div>
<?php
if (get_field('purchasable', $id)) {
  // Presence of an attachment overrides external URL:
  $file = get_field('preview', $id);
  if ($file) $url = $file['url'];
  if (!$url) $url = get_field('preview_url', $id);
  $link = get_field('link', $id);
?>
  <div style="display: none;">
    <div id="preview-content-<?php echo $id; ?>">
      <h2 style="font-size: 18px; color: #6D6E71;">Preview Content:  <?php echo $title; ?></h2>
      <iframe src="http://docs.google.com/gview?embedded=true&url=<?php echo esc_url($url); ?>" width="800" height="400" style="margin: 20px 0; border: 1px inset #eee;"></iframe>
      <p style="text-align: right;"><strong><a href="<?php echo esc_url($link); ?>" target="_blank">Purchase This Publication</a></strong></p>
    </div>
  </div>
</li>
<?php
  }
}

function renderResources($taxonomy_term_id) {
  $posts = getResources($taxonomy_term_id);
  if (count($posts) > 0) {
    echo "<ul>";
		foreach ($posts as $post){
      renderResource($post);
    }
    echo "</ul>";
  }
}
?>

<div id="primary">
    <div id="content" role="main">

		<?php if (function_exists('simple_breadcrumb')) { simple_breadcrumb(); } ?>

        <article id="post-<?php the_post(); the_ID(); ?>" <?php post_class(); ?>>
            <header class="entry-header">
                <h1 class="entry-title"><?php the_title(); ?></h1>
            </header>
            <!-- .entry-header -->

            <div class="entry-content">
              <?php the_content(); ?>

              <p><a name="articles-white-papers"></a></p>
              <h2 style="padding-bottom: 8px; border-bottom: 1px solid #eee;">White Papers</h2>
              <?php renderResources(10); ?>

              <p><a name="articles-white-papers"></a></p>
              <h2 style="padding-bottom: 8px; border-bottom: 1px solid #eee;">Quick Links</h2>
              <?php renderResources(11); ?>


          </div>
        </article>
    </div>
    <!-- #content -->
</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
